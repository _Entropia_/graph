package graph.attribute;

/**
 * Default Attribute implementation
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public final class DefaultAttribute<T> implements Attribute<T> {
	private final String name;
	private final T defaultValue;

	/**
	 * Creates a new DefaultAttribute.
	 * 
	 * @param name
	 *            Name of the attribute
	 * @param defaultValue
	 *            Default value for this attribute
	 */
	public DefaultAttribute(String name, T defaultValue) {
		this.name = name;
		this.defaultValue = defaultValue;
	}

	@Override
	public String getAttributeName() {
		return name;
	}

	@Override
	public T getDefaultValue() {
		return defaultValue;
	}

	@Override
	public String toString() {
		return getAttributeName();
	}
}