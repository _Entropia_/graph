package graph.attribute;

/**
 * Interface for Attributes describing properties of a graph.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Type of attribute
 */
public interface Attribute<T> {
	/**
	 * @return The name of the attribute
	 */
	String getAttributeName();

	/**
	 * @return The attribute's default value
	 */
	T getDefaultValue();
}
