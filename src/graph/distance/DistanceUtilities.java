package graph.distance;

import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graph.attribute.Attribute;
import graph.attribute.DefaultAttribute;

/**
 * Utility functions for computing distances in graphs.
 * 
 * @author Corny
 */
public class DistanceUtilities {
	/**
	 * The distance of a vertex to the source.
	 */
	public static final Attribute<Double> DISTANCE = new DefaultAttribute<Double>("Distance", Double.POSITIVE_INFINITY);

	/**
	 * Total cost of reaching a target by traversing this vertex.
	 */
	public static final Attribute<Double> F_SCORE = new DefaultAttribute<Double>("Distance", Double.POSITIVE_INFINITY);

	/**
	 * Previous vertex on the shortest path to this vertex.
	 */
	public static final Attribute<Object> PREVIOUS = new DefaultAttribute<Object>("Previous Vertex", null);

	/**
	 * Previous vertex on the shortest path to this vertex.
	 */
	public static final Attribute<Boolean> EVALUATED = new DefaultAttribute<Boolean>("Was Evaluated", false);

	/**
	 * Runs Dijkstra's algorithm on a graph.
	 * 
	 * @param graph
	 *            Graph in which edge weights represent distances.
	 * @param source
	 *            Start vertex for distances
	 */
	public static <T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>> void dijkstra(Graph<T, EDGE, VERTEX> graph,
			VERTEX source) {
		source.setAttributeValue(DISTANCE, 0.);

		PriorityQueue<VERTEX> unvisited = new PriorityQueue<>(graph.getVertexCount(), new Comparator<VERTEX>() {
			@Override
			public int compare(VERTEX o1, VERTEX o2) {
				return o1.getAttributeValue(DISTANCE).compareTo(o2.getAttributeValue(DISTANCE));
			}
		});

		for (VERTEX v : graph) {
			unvisited.add(v);
		}

		while (!unvisited.isEmpty()) {
			VERTEX vMin = unvisited.poll();
			double minDist = vMin.getAttributeValue(DISTANCE);
			for (Edge<T> edge : vMin) {
				@SuppressWarnings("unchecked")
				VERTEX vOther = (VERTEX) edge.getEnd();
				double oldDist = vOther.getAttributeValue(DISTANCE);
				double newDist = minDist + edge.getWeight();
				if (newDist < oldDist) {
					unvisited.remove(vOther);
					vOther.setAttributeValue(DISTANCE, newDist);
					vOther.setAttributeValue(PREVIOUS, vMin);
					unvisited.add(vOther);
				}
			}
		}
	}

	/**
	 * Computes the shortest path between two vertices using A*-algorithm (see
	 * https://en.wikipedia.org/wiki/A*_search_algorithm).
	 * 
	 * @param graph
	 *            Graph in which edge weights represent distances.
	 * @param source
	 *            Start vertex of path
	 * @param target
	 *            Target vertex of path
	 */
	public static <T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>> List<VERTEX> shortestPath(
			Graph<T, EDGE, VERTEX> graph, VERTEX source, VERTEX target) {
		source.setAttributeValue(DISTANCE, 0.);

		PriorityQueue<VERTEX> queue = new PriorityQueue<>(graph.getVertexCount(), new Comparator<VERTEX>() {
			@Override
			public int compare(VERTEX o1, VERTEX o2) {
				return o1.getAttributeValue(F_SCORE).compareTo(o2.getAttributeValue(F_SCORE));
			}
		});

		queue.add(source);
		while (!queue.isEmpty()) {
			// current is the element by which target can be reached using
			// minimal weight
			VERTEX current = queue.poll();
			if (current == target) {
				// TODO construct path
				return null;
			}

			// only evaluate vertices that weren't evaluated before.
			if (current.getAttributeValue(EVALUATED))
				continue;
			// update distances of neighbors
			double minDist = current.getAttributeValue(DISTANCE);
			for (Edge<T> edge : current) {
				@SuppressWarnings("unchecked")
				VERTEX vOther = (VERTEX) edge.getEnd();
				double oldDist = vOther.getAttributeValue(DISTANCE);
				double newDist = minDist + edge.getWeight();
				if (newDist < oldDist) {
					vOther.setAttributeValue(DISTANCE, newDist);
					vOther.setAttributeValue(PREVIOUS, current);
				}
				if (!queue.contains(vOther))
					queue.add(vOther);
			}
		}
		return null;
	}
}
