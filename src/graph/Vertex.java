package graph;

import graph.attribute.Attribute;

import java.util.ListIterator;
import java.util.Map;

/**
 * Interface for graph vertices.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 */
public interface Vertex<T, EDGE extends Edge<T>> extends Iterable<EDGE>,
		Comparable<Vertex<T, ?>> {
	/**
	 * @return The label of this vertex
	 */
	T getLabel();

	/**
	 * @return A textual description of this vertex and its outgoing edges
	 */
	String getLongDescription();

	/**
	 * @return An iterator over the vertex's outgoing edges
	 */
	ListIterator<EDGE> listIterator();

	/**
	 * @param index
	 *            index to start the iteration
	 * @return An iterator over the vertex's outgoing edges
	 */
	ListIterator<EDGE> listIterator(int index);

	/**
	 * @return The number of outgoing edges
	 */
	int getEdgeCount();

	/**
	 * @return A map of attributes
	 */
	Map<Attribute<?>, ?> getAttributes();

	/**
	 * Returns the value for a given attribute or the default value, if the
	 * attribute is not set.
	 * 
	 * @param attribute
	 *            Attribute
	 * @return Attribute value
	 */
	<ValueType> ValueType getAttributeValue(Attribute<ValueType> attribute);

	/**
	 * Sets an attribute value.
	 * 
	 * @param attribute
	 *            Attribute
	 * @param value
	 *            value
	 */
	<ValueType> void setAttributeValue(Attribute<ValueType> attribute,
			ValueType value);
}
