package graph.defaultgraph;

import graph.Edge;
import graph.Vertex;
import graph.attribute.Attribute;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.Iterator;
import java.util.List;
import java.util.ListIterator;
import java.util.Map;

/**
 * Default, generic vertex implementation
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 */
public class DefaultVertex<T extends Comparable<T>, EDGE extends Edge<T>>
		implements Vertex<T, EDGE> {
	private final T label;
	private final ArrayList<EDGE> edges = new ArrayList<EDGE>();
	private final HashMap<Attribute<?>, Object> attributes = new HashMap<Attribute<?>, Object>();

	/**
	 * Creates a new vertex
	 * 
	 * @param label
	 *            Label
	 */
	public DefaultVertex(T label) {
		this.label = label;
	}

	@Override
	public Iterator<EDGE> iterator() {
		return edges.iterator();
	}

	@Override
	public T getLabel() {
		return label;
	}

	/**
	 * @return A list of outgoing edges
	 */
	public List<EDGE> getEdges() {
		return edges;
	}

	@Override
	public Map<Attribute<?>, ?> getAttributes() {
		return attributes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <ValueType> ValueType getAttributeValue(
			Attribute<ValueType> attribute) {
		ValueType value = (ValueType) attributes.get(attribute);
		return value == null ? attribute.getDefaultValue() : value;
	}

	@Override
	public <ValueType> void setAttributeValue(Attribute<ValueType> attribute,
			ValueType value) {
		attributes.put(attribute, value);
	}

	/**
	 * Adds an outgoing edge to this vertex.
	 * 
	 * @param edge
	 *            Outgoing edge
	 * @throws IllegalArgumentException
	 *             If the start vertex of the edge is not this vertex
	 */
	public void addEdge(EDGE edge) {
		if (edge.getStart() != this) {
			throw new IllegalArgumentException(
					"Edge does not start with this vertex.");
		}
		edges.add(edge);
	}

	public String getLongDescription() {
		StringBuilder build = new StringBuilder("Vertex \"");
		build.append(getLabel());
		build.append("\"");
		for (Edge<T> e : this) {
			build.append('\n');
			build.append('\t');
			build.append(e);
		}
		for (Attribute<?> a : getAttributes().keySet()) {
			build.append('\n');
			build.append('\t');
			build.append(a.getAttributeName());
			build.append(": ");
			build.append(getAttributeValue(a));
		}
		return build.toString();
	}

	@Override
	public String toString() {
		return "Vertex \"" + getLabel() + "\"";
	}

	@Override
	public ListIterator<EDGE> listIterator() {
		return edges.listIterator();
	}

	@Override
	public ListIterator<EDGE> listIterator(int index) {
		return edges.listIterator(index);
	}

	@Override
	public int getEdgeCount() {
		return edges.size();
	}

	@Override
	public int compareTo(Vertex<T, ?> o) {
		return getLabel().compareTo(o.getLabel());
	}
}
