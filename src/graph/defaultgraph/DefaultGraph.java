package graph.defaultgraph;

import graph.Edge;
import graph.Graph;
import graph.Vertex;

import java.util.ArrayList;
import java.util.Iterator;
import java.util.List;

/**
 * Default, generic graph implementation.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 * @param <VERTEX>
 *            Vertex type
 */
public class DefaultGraph<T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>>
		implements Graph<T, EDGE, VERTEX> {
	private final List<VERTEX> vertices = new ArrayList<VERTEX>();

	/**
	 * @return List of vertexes
	 */
	public List<VERTEX> getVertices() {
		return vertices;
	}
	
	@Override
	public int getVertexCount() {
		return vertices.size();
	}

	/**
	 * Adds a vertex to the graph.
	 * 
	 * @param vertex
	 *            New vertex
	 */
	public void addVertex(VERTEX vertex) {
		vertices.add(vertex);
	}

	@Override
	public Iterator<VERTEX> iterator() {
		return vertices.iterator();
	}

	@Override
	public String toString() {
		StringBuilder build = new StringBuilder();
		boolean first = true;
		for (Vertex<T, EDGE> v : this) {
			if (!first) {
				build.append('\n');
			} else {
				first = false;
			}
			build.append(v.getLongDescription());
		}
		return build.toString();
	}
}
