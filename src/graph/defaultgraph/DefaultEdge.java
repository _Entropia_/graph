package graph.defaultgraph;

import graph.Edge;
import graph.Vertex;
import graph.attribute.Attribute;

import java.util.HashMap;
import java.util.Map;

/**
 * Default, generic edge implementation
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class DefaultEdge<T> implements Edge<T> {
	private final Vertex<T, ? extends Edge<T>> start, end;
	private final double weight;
	private final HashMap<Attribute<?>, Object> attributes = new HashMap<Attribute<?>, Object>();

	/**
	 * Creates a new edge
	 * 
	 * @param start
	 *            Start vertex
	 * @param end
	 *            Target vertex
	 * @param weight
	 *            Weight
	 */
	public DefaultEdge(Vertex<T, ? extends Edge<T>> start,
			Vertex<T, ? extends Edge<T>> end, double weight) {
		this.start = start;
		this.end = end;
		this.weight = weight;
	}

	@Override
	public Vertex<T, ? extends Edge<T>> getStart() {
		return start;
	}

	@Override
	public Vertex<T, ? extends Edge<T>> getEnd() {
		return end;
	}

	@Override
	public double getWeight() {
		return weight;
	}

	@Override
	public String toString() {
		return "Edge " + start.getLabel() + " -> " + end.getLabel()
				+ " weight " + getWeight();
	}

	@Override
	public Map<Attribute<?>, ?> getAttributes() {
		return attributes;
	}

	@SuppressWarnings("unchecked")
	@Override
	public <ValueType> ValueType getAttributeValue(
			Attribute<ValueType> attribute) {
		ValueType value = (ValueType) attributes.get(attribute);
		return value == null ? attribute.getDefaultValue() : value;
	}

	@Override
	public <ValueType> void setAttributeValue(Attribute<ValueType> attribute,
			ValueType value) {
		attributes.put(attribute, value);
	}
}
