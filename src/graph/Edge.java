package graph;

import graph.attribute.Attribute;

import java.util.Map;

/**
 * Interface for (directed) graph edges.
 * 
 * @author Corny
 * 
 * @param <T>
 */
public interface Edge<T> {
	/**
	 * @return The start vertex of this edge
	 */
	Vertex<T, ? extends Edge<T>> getStart();

	/**
	 * @return The target vertex of this edge
	 */
	Vertex<T, ? extends Edge<T>> getEnd();

	/**
	 * @return The weight of this edge
	 */
	double getWeight();

	/**
	 * @return A map of attributes
	 */
	Map<Attribute<?>, ?> getAttributes();

	/**
	 * Returns the value for a given attribute or the default value, if the
	 * attribute is not set.
	 * 
	 * @param attribute
	 *            Attribute
	 * @return Attribute value
	 */
	<ValueType> ValueType getAttributeValue(Attribute<ValueType> attribute);

	/**
	 * Sets an attribute value.
	 * 
	 * @param attribute
	 *            Attribute
	 * @param value
	 *            value
	 */
	<ValueType> void setAttributeValue(Attribute<ValueType> attribute,
			ValueType value);
}
