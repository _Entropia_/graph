package graph.flow.fordfulkerson;

import graph.flow.FlowNetwork;
import graph.utils.GraphUtilities;

/**
 * Finds augmenting paths using breadth first search.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public final class BFSAugmentingPathFinder<T extends Comparable<T>> extends
		GraphSearchAugmentingPathFinder<T> {
	/**
	 * Creates a new BFSAugmentingPathFinder.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 */
	public BFSAugmentingPathFinder(FlowNetwork<T> flowNetwork) {
		super(flowNetwork);
	}

	@Override
	protected void startSearch() {
		GraphUtilities.breadthFirstSearch(getFlowNetwork().getReducedNetwork(),
				getFlowNetwork().getReducedNetwork().getSource(), this);
	}
}