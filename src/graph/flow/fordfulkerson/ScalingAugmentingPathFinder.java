package graph.flow.fordfulkerson;

import java.util.Iterator;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowVertex;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;
import graph.utils.GraphSearchAdapter;
import graph.utils.GraphUtilities;
import graph.utils.PreviousVertexIterator;

/**
 * Finds augmenting paths using the scaling method by Ahuja and Orlin.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public final class ScalingAugmentingPathFinder<T extends Comparable<T>> extends
		GraphSearchAdapter<T, ReducedNetworkEdge<T>, ReducedNetworkVertex<T>>
		implements AugmentingPathFinder<T> {
	private final FlowNetwork<T> flowNetwork;
	private final double minFlow;

	private boolean foundPath = false;
	private double K;

	/**
	 * Creates a new BFSAugmentingPathFinder.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 * @param minFlow
	 *            Minimum possible flow (usually 1)
	 */
	public ScalingAugmentingPathFinder(FlowNetwork<T> flowNetwork,
			double minFlow) {
		this.flowNetwork = flowNetwork;
		this.minFlow = minFlow;

		// Find highest capacity
		double maxCap = 0;
		for (FlowVertex<T> v : flowNetwork) {
			for (FlowEdge<T> e : v) {
				if (e.getCapacity() > maxCap) {
					maxCap = e.getCapacity();
				}
			}
		}

		K = Math.pow(2, Math.floor(Math.log(maxCap) / Math.log(2)));
	}

	@Override
	public boolean hasFoundPath() {
		return foundPath;
	}

	@Override
	public void searchPath() {
		foundPath = false;
		while (K >= minFlow && !foundPath) {
			GraphUtilities.breadthFirstSearch(getFlowNetwork()
					.getReducedNetwork(), getFlowNetwork().getReducedNetwork()
					.getSource(), this);
			if (!foundPath) {
				K /= 2;
			}
		}
	}

	protected FlowNetwork<T> getFlowNetwork() {
		return flowNetwork;
	}

	@Override
	public Iterator<ReducedNetworkEdge<T>> iterator() {
		return new PreviousVertexIterator<T, ReducedNetworkEdge<T>, ReducedNetworkVertex<T>>(
				flowNetwork.getTarget().getReducedVertex());
	}

	@Override
	public boolean vertexVisited(ReducedNetworkVertex<T> vertex) {
		if (vertex.getFlowVertex() == flowNetwork.getTarget()) {
			// Finished traversal until target
			foundPath = true;
			return false;
		}
		return true;
	}

	@Override
	public boolean whiteVertexDetected(ReducedNetworkVertex<T> current,
			ReducedNetworkVertex<T> whiteVertex, ReducedNetworkEdge<T> edge) {
		if (edge.getRemainingCapacity() < K) {
			// Don't follow edges with a loo low remaining capacity
			return false;
		}
		return true;
	}

}