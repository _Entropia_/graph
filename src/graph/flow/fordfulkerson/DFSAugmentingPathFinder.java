package graph.flow.fordfulkerson;

import graph.flow.FlowNetwork;
import graph.utils.GraphUtilities;

/**
 * Finds augmenting paths using depth first search.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public final class DFSAugmentingPathFinder<T extends Comparable<T>> extends
		GraphSearchAugmentingPathFinder<T> {
	/**
	 * Creates a new DFSAugmentingPathFinder.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 */
	public DFSAugmentingPathFinder(FlowNetwork<T> flowNetwork) {
		super(flowNetwork);
	}

	@Override
	protected void startSearch() {
		GraphUtilities.depthFirstSearch(getFlowNetwork().getReducedNetwork(),
				getFlowNetwork().getReducedNetwork().getSource(), true, this);
	}
}