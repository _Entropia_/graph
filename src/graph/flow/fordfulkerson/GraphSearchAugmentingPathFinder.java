package graph.flow.fordfulkerson;

import java.util.Iterator;

import graph.flow.FlowNetwork;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;
import graph.utils.GraphSearchAdapter;
import graph.utils.PreviousVertexIterator;

abstract class GraphSearchAugmentingPathFinder<T extends Comparable<T>> extends
		GraphSearchAdapter<T, ReducedNetworkEdge<T>, ReducedNetworkVertex<T>>
		implements AugmentingPathFinder<T> {

	private final FlowNetwork<T> flowNetwork;
	private boolean foundPath = false;

	GraphSearchAugmentingPathFinder(FlowNetwork<T> flowNetwork) {
		this.flowNetwork = flowNetwork;
	}

	@Override
	public boolean hasFoundPath() {
		return foundPath;
	}
	
	@Override
	public void searchPath() {
		foundPath = false;
		startSearch();
	}

	protected abstract void startSearch();
	
	protected FlowNetwork<T> getFlowNetwork() {
		return flowNetwork;
	}
	
	@Override
	public Iterator<ReducedNetworkEdge<T>> iterator() {
		return new PreviousVertexIterator<T, ReducedNetworkEdge<T>, ReducedNetworkVertex<T>>(
				flowNetwork.getTarget().getReducedVertex());
	}

	@Override
	public boolean vertexVisited(ReducedNetworkVertex<T> vertex) {
		if (vertex.getFlowVertex() == flowNetwork.getTarget()) {
			// Finished traversal until target
			foundPath = true;
			return false;
		}
		return true;
	}

	@Override
	public boolean whiteVertexDetected(ReducedNetworkVertex<T> current,
			ReducedNetworkVertex<T> whiteVertex, ReducedNetworkEdge<T> edge) {
		if (edge.getRemainingCapacity() <= 0) {
			// Don't follow edges with zero remaining capacity
			return false;
		}
		return true;
	}

}