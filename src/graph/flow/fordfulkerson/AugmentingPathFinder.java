package graph.flow.fordfulkerson;

import graph.flow.ReducedNetworkEdge;

/**
 * Interface for classes to find augmenting paths in Reduced Networks.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public interface AugmentingPathFinder<T extends Comparable<T>> extends
		Iterable<ReducedNetworkEdge<T>> {

	/**
	 * Starts a search for an augmenting path
	 */
	void searchPath();

	/**
	 * @return true, if an augmenting path was found.
	 */
	boolean hasFoundPath();
}