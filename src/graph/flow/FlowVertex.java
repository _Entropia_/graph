package graph.flow;

import graph.defaultgraph.DefaultVertex;

/**
 * A vertex in a Flow Network.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class FlowVertex<T extends Comparable<T>> extends DefaultVertex<T, FlowEdge<T>> {
	private ReducedNetworkVertex<T> reducedVertex;

	/**
	 * Creates a new FlowVertex with a given label.
	 * 
	 * @param label
	 *            Label of this vertex
	 */
	public FlowVertex(T label) {
		super(label);
	}

	/**
	 * @return The vertex in the corresponding Reduced Network.
	 */
	public ReducedNetworkVertex<T> getReducedVertex() {
		if (reducedVertex == null) {
			reducedVertex = new ReducedNetworkVertex<T>(this);
		}
		return reducedVertex;
	}

}
