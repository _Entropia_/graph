package graph.flow;

import graph.attribute.Attribute;
import graph.attribute.DefaultAttribute;
import graph.flow.fordfulkerson.AugmentingPathFinder;
import graph.flow.preflowpush.PreflowPush;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.IOException;
import java.io.PrintStream;
import java.util.Scanner;

/**
 * Utilities for Flow Networks.
 * 
 * @author Corny
 */
public class FlowUtilities {
	/**
	 * The height of a vertex in a Flow Network during a Preflow-Push algorithm
	 */
	public static final Attribute<Integer> HEIGHT = new DefaultAttribute<Integer>(
			"Height", 0);

	/**
	 * The excess of a vertex in a Flow Network during a Preflow-Push algorithm
	 */
	public static final Attribute<Double> EXCESS = new DefaultAttribute<Double>(
			"Excess", 0.);

	/**
	 * Reads a Flow Network from a file.
	 * 
	 * @param file
	 *            Input file
	 * @return Flow Network
	 * @throws IOException
	 *             If the input file does not exist or is corrupt.
	 */
	public static FlowNetwork<Integer> readFlowNetwork(File file)
			throws IOException {
		if (!file.exists()) {
			throw new FileNotFoundException("File not Found!!");
		}

		Scanner scanner = new Scanner(new FileInputStream(file));
		int numVertices = scanner.nextInt();
		int numEdges = scanner.nextInt();
		int sourceIndex = scanner.nextInt();
		int targetIndex = scanner.nextInt();

		FlowNetwork<Integer> network = new FlowNetwork<Integer>();
		for (int i = 1; i <= numVertices; ++i) {
			network.addVertex(new FlowVertex<Integer>(i));
		}

		network.setSource(network.getVertices().get(sourceIndex - 1));
		network.setTarget(network.getVertices().get(targetIndex - 1));

		while (numEdges > 0) {
			int startIndex = scanner.nextInt();
			int endIndex = scanner.nextInt();
			double capForward;
			double capBackward;
			if (scanner.hasNextDouble()) {
				capForward = scanner.nextDouble();
			} else {
				capForward = scanner.nextInt();
			}
			if (scanner.hasNextDouble()) {
				capBackward = scanner.nextDouble();
			} else {
				capBackward = scanner.nextInt();
			}

			network.addEdge(network.getVertices().get(startIndex - 1), network
					.getVertices().get(endIndex - 1), capForward, capBackward);

			--numEdges;
			if (!scanner.hasNextInt()) {
				if (numEdges > 0) {
					System.err
							.println("WARNING: File contains less edges than specified.");
				}
				break;
			}
		}

		return network;
	}

	/**
	 * Runs the Ford-Fulkerson algorithm on a Flow Network to discover the
	 * maximum flow in the graph.
	 * 
	 * @param graph
	 *            Flow Network
	 * @param pathFinder
	 *            Augmenting path finder
	 * @param log
	 *            Stream to print log to, or null to print no log
	 * @return Number of augmenting paths until the algorhtm terminated
	 */
	public static <T extends Comparable<T>> int fordFulkerson(
			FlowNetwork<T> graph, AugmentingPathFinder<T> pathFinder,
			PrintStream log) {
		int augmentingPaths = 0;

		while (true) {
			// Find augmenting path
			pathFinder.searchPath();

			if (!pathFinder.hasFoundPath()) {
				// No path exists
				break;
			}

			++augmentingPaths;

			double minCapacity = Double.MAX_VALUE;

			if (log != null) {
				log.print("Neuer augmentierender Weg: ");
			}
			for (ReducedNetworkEdge<T> prevEdge : pathFinder) {
				if (log != null) {
					log.print(prevEdge.getEnd().getLabel() + "->"
							+ prevEdge.getStart().getLabel() + " ");
				}
				if (prevEdge.getRemainingCapacity() < minCapacity) {
					minCapacity = prevEdge.getRemainingCapacity();
				}
			}
			if (log != null) {
				log.println("; min flow: " + minCapacity);
			}

			// Update flow values on path
			for (ReducedNetworkEdge<T> prevEdge : pathFinder) {
				prevEdge.updateFlow(minCapacity);
			}
		}
		return augmentingPaths;
	}

	/**
	 * Runs a Preflow-Push algorithm on a Flow Network to discover the maximum
	 * flow in the graph.
	 * 
	 * @param graph
	 *            Flow Network
	 * @param pushAlgorithm
	 *            Preflow-Push algorithm
	 */
	public static <T extends Comparable<T>> void preflowPush(
			FlowNetwork<T> graph, PreflowPush<T> pushAlgorithm) {
		// Initialization
		for (FlowVertex<T> v : graph) {
			v.setAttributeValue(HEIGHT, 0);
			v.setAttributeValue(EXCESS, 0.);
			for (FlowEdge<T> e : v) {
				e.setFlow(0.);
			}
		}
		graph.getSource().setAttributeValue(HEIGHT, graph.getVertexCount());

		for (ReducedNetworkEdge<T> sourceEdge : graph.getSource()
				.getReducedVertex()) {
			sourceEdge.getEnd().setAttributeValue(EXCESS,
					sourceEdge.getRemainingCapacity());
			sourceEdge.updateFlow(sourceEdge.getRemainingCapacity());
		}

		// Push and Lift
		pushAlgorithm.startPreflowPush();
	}
}
