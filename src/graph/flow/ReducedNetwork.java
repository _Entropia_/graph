package graph.flow;

import graph.Graph;

import java.util.Iterator;

/**
 * Reduced Network corresponding to a Flow Network.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class ReducedNetwork<T extends Comparable<T>> implements
		Graph<T, ReducedNetworkEdge<T>, ReducedNetworkVertex<T>> {
	private final FlowNetwork<T> flowNetwork;

	/**
	 * Creates a new Reduced Network from a Flow Network.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 */
	ReducedNetwork(FlowNetwork<T> flowNetwork) {
		this.flowNetwork = flowNetwork;
	}
	
	@Override
	public int getVertexCount() {
		return flowNetwork.getVertexCount();
	}

	/**
	 * @return The source vertex in this Reduced Network.
	 */
	public ReducedNetworkVertex<T> getSource() {
		return flowNetwork.getSource().getReducedVertex();
	}

	/**
	 * @return The target vertex in this Reduced Network.
	 */
	public ReducedNetworkVertex<T> getTarget() {
		return flowNetwork.getTarget().getReducedVertex();
	}

	@Override
	public Iterator<ReducedNetworkVertex<T>> iterator() {
		return new ReducedNetworkIterator();
	}

	private final class ReducedNetworkIterator implements
			Iterator<ReducedNetworkVertex<T>> {
		private Iterator<FlowVertex<T>> originalIt;

		public ReducedNetworkIterator() {
			originalIt = flowNetwork.iterator();
		}

		@Override
		public boolean hasNext() {
			return originalIt.hasNext();
		}

		@Override
		public ReducedNetworkVertex<T> next() {
			return originalIt.next().getReducedVertex();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException(
					"Cannot remove vertices from reduced network.");
		}

	}
}
