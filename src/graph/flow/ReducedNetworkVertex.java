package graph.flow;

import graph.Vertex;
import graph.attribute.Attribute;

import java.util.Iterator;
import java.util.ListIterator;
import java.util.Map;

/**
 * A Vertex in a reduced network
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class ReducedNetworkVertex<T extends Comparable<T>> implements
		Vertex<T, ReducedNetworkEdge<T>> {
	private final FlowVertex<T> flowVertex;

	ReducedNetworkVertex(FlowVertex<T> originalVertex) {
		this.flowVertex = originalVertex;
	}

	@Override
	public Iterator<ReducedNetworkEdge<T>> iterator() {
		return new ReducedNetworkVertexIterator();
	}

	@Override
	public T getLabel() {
		return flowVertex.getLabel();
	}

	/**
	 * @return The vertex in the corresponding Flow Network.
	 */
	public FlowVertex<T> getFlowVertex() {
		return flowVertex;
	}

	@Override
	public Map<Attribute<?>, ?> getAttributes() {
		return flowVertex.getAttributes();
	}

	@Override
	public <ValueType> ValueType getAttributeValue(
			Attribute<ValueType> attribute) {
		return flowVertex.getAttributeValue(attribute);
	}

	@Override
	public <ValueType> void setAttributeValue(Attribute<ValueType> attribute,
			ValueType value) {
		flowVertex.setAttributeValue(attribute, value);
	}

	@Override
	public String getLongDescription() {
		return flowVertex.getLongDescription();
	}

	@Override
	public ListIterator<ReducedNetworkEdge<T>> listIterator() {
		return new ReducedNetworkVertexIterator();
	}

	@Override
	public ListIterator<ReducedNetworkEdge<T>> listIterator(int index) {
		return new ReducedNetworkVertexIterator(index);
	}

	@Override
	public int getEdgeCount() {
		return flowVertex.getEdgeCount();
	}

	@Override
	public String toString() {
		return flowVertex.toString();
	}

	@Override
	public int compareTo(Vertex<T, ?> o) {
		return getLabel().compareTo(o.getLabel());
	}

	private final class ReducedNetworkVertexIterator implements
			ListIterator<ReducedNetworkEdge<T>> {
		private ListIterator<FlowEdge<T>> originalIt;

		public ReducedNetworkVertexIterator(int index) {
			originalIt = flowVertex.listIterator(index);
		}

		public ReducedNetworkVertexIterator() {
			this(0);
		}

		@Override
		public ReducedNetworkEdge<T> next() {
			return originalIt.next().getReducedEdge();
		}

		@Override
		public ReducedNetworkEdge<T> previous() {
			return originalIt.previous().getReducedEdge();
		}

		@Override
		public boolean hasNext() {
			return originalIt.hasNext();
		}

		@Override
		public boolean hasPrevious() {
			return originalIt.hasPrevious();
		}

		@Override
		public int nextIndex() {
			return originalIt.nextIndex();
		}

		@Override
		public int previousIndex() {
			return originalIt.previousIndex();
		}

		@Override
		public void remove() {
			throw new UnsupportedOperationException(
					"Cannot remove edges from reduced network.");
		}

		@Override
		public void set(ReducedNetworkEdge<T> e) {
			throw new UnsupportedOperationException(
					"Cannot modify reduced network.");
		}

		@Override
		public void add(ReducedNetworkEdge<T> e) {
			throw new UnsupportedOperationException(
					"Cannot modify reduced network.");
		}

	}

}
