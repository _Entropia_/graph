package graph.flow;

import graph.defaultgraph.DefaultGraph;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Represents a Flow Network. It contains backward edges for each directed edge.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class FlowNetwork<T extends Comparable<T>> extends DefaultGraph<T, FlowEdge<T>, FlowVertex<T>> {
	private enum IterationState {
		SOURCE, INNER, END;
	}

	private FlowVertex<T> source, target;
	private ReducedNetwork<T> reducedNetwork;

	/**
	 * Sets the source vertex for this Flow Network. The vertex must be part of
	 * this graph.
	 * 
	 * This method does not check whether the vertex is a valid source vertex
	 * (i.e., the vertex has no incoming edges)
	 * 
	 * @param source
	 *            New source vertex
	 * @throws IllegalArgumentException
	 *             If the vertex is not part of this graph.
	 */
	public void setSource(FlowVertex<T> source) {
		if (!getVertices().contains(source)) {
			throw new IllegalArgumentException(
					"Source is not a vertex in this graph.");
		}
		this.source = source;
	}

	/**
	 * Sets the target vertex for this Flow Network. The vertex must be part of
	 * this graph.
	 * 
	 * If the vertex is not a valid target vertex (i.e., the number of outgoing
	 * edges is zero), an exception is thrown.
	 * 
	 * @param target
	 *            New target vertex
	 * @throws IllegalArgumentException
	 *             If the vertex is not part of this graph or is no valid target
	 *             vertex
	 */
	public void setTarget(FlowVertex<T> target) {
		if (!getVertices().contains(target)) {
			throw new IllegalArgumentException(
					"Target is not a vertex in this graph.");
		}
		/*if (target.getEdgeCount() > 0) {
			throw new IllegalArgumentException("The vertex " + target
					+ " is no valid target vertex. It has outgoing edges.");
		}*/
		this.target = target;
	}

	/**
	 * @return The source vertex of this Flow Network
	 */
	public FlowVertex<T> getSource() {
		return source;
	}

	/**
	 * @return The target vertex of this Flow Network
	 */
	public FlowVertex<T> getTarget() {
		return target;
	}

	/**
	 * Adds two edges between two vertices of this flow network, one for each
	 * direction. This method does not check whether edges between these
	 * vertices already exist.
	 * 
	 * @param start
	 *            Start vertex
	 * @param end
	 *            End vertex
	 * @param capForward
	 *            Capacity of the forward edge
	 * @param capBackward
	 *            Capacity of the backward edge
	 */
	public void addEdge(FlowVertex<T> start, FlowVertex<T> end,
			double capForward, double capBackward) {
		// TODO Check whether edge already exists
		FlowEdge<T> forwardEdge = new FlowEdge<T>(start, end, capForward, 0);
		FlowEdge<T> backwardEdge = new FlowEdge<T>(end, start, capBackward, 0);
		forwardEdge.setBackwardEdge(backwardEdge);
		backwardEdge.setBackwardEdge(forwardEdge);
		start.addEdge(forwardEdge);
		end.addEdge(backwardEdge);
	}

	/**
	 * @return The Reduced Network corresponding to this Flow Network.
	 */
	public ReducedNetwork<T> getReducedNetwork() {
		if (reducedNetwork == null) {
			reducedNetwork = new ReducedNetwork<T>(this);
		}
		return reducedNetwork;
	}

	@Override
	public Iterator<FlowVertex<T>> iterator() {
		final Iterator<FlowVertex<T>> parentIt = super.iterator();
		return new Iterator<FlowVertex<T>>() {
			private IterationState state = IterationState.SOURCE;

			@Override
			public boolean hasNext() {
				return state != IterationState.END;
			}

			@Override
			public FlowVertex<T> next() {
				switch (state) {
				case SOURCE:
					state = IterationState.INNER;
					return source;
				case INNER:
					while (parentIt.hasNext()) {
						FlowVertex<T> next = parentIt.next();
						if (next != source && next != target) {
							return next;
						}
					}
					state = IterationState.END;
					return target;
				case END:
				default:
					throw new NoSuchElementException("Iteration has ended.");
				}
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException(
						"Cannot remove vertices from graph.");
			}
		};
	}
}
