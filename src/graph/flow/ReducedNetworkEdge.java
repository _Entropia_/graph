package graph.flow;

import graph.Edge;
import graph.Vertex;
import graph.attribute.Attribute;

import java.util.Map;

/**
 * An edge in a reduced Network.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class ReducedNetworkEdge<T extends Comparable<T>> implements Edge<T> {
	private final FlowEdge<T> flowEdge;

	ReducedNetworkEdge(FlowEdge<T> flowEdge) {
		this.flowEdge = flowEdge;
	}

	@SuppressWarnings("unchecked")
	@Override
	public Vertex<T, ? extends Edge<T>> getStart() {
		return ((FlowVertex<T>) flowEdge.getStart()).getReducedVertex();
	}

	@SuppressWarnings("unchecked")
	@Override
	public Vertex<T, ? extends Edge<T>> getEnd() {
		return ((FlowVertex<T>) flowEdge.getEnd()).getReducedVertex();
	}

	@Override
	public double getWeight() {
		return flowEdge.getCapacity() - flowEdge.getFlow();
	}

	/**
	 * @return The edge in the corresponding Flow Network.
	 */
	public FlowEdge<T> getFlowEdge() {
		return flowEdge;
	}

	/**
	 * @return The remaining capacity of this edge. Equal to
	 *         {@link #getWeight()}.
	 * @see #getWeight()
	 */
	public double getRemainingCapacity() {
		return getWeight();
	}

	/**
	 * Updates the flow in the underlying Flow Network.
	 * 
	 * @param flowIncrement
	 *            Value to add to the flow of the underlying edge, or a negative
	 *            value to decrement the flow value.
	 */
	public void updateFlow(double flowIncrement) {
		flowEdge.setFlow(flowEdge.getFlow() + flowIncrement);
		flowEdge.getBackwardEdge().setFlow(-flowEdge.getFlow());
	}

	@Override
	public Map<Attribute<?>, ?> getAttributes() {
		return flowEdge.getAttributes();
	}

	@Override
	public <ValueType> ValueType getAttributeValue(
			Attribute<ValueType> attribute) {
		return flowEdge.getAttributeValue(attribute);
	}

	@Override
	public <ValueType> void setAttributeValue(Attribute<ValueType> attribute,
			ValueType value) {
		flowEdge.setAttributeValue(attribute, value);
	}

	@Override
	public String toString() {
		return flowEdge.toString();
	}

}
