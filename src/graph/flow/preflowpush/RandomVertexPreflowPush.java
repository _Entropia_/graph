package graph.flow.preflowpush;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.FlowVertex;
import graph.flow.ReducedNetworkVertex;

import java.io.PrintStream;
import java.util.ArrayList;
import java.util.Random;

/**
 * Preflow-Push algorithm which randomly picks an active vertex.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class RandomVertexPreflowPush<T extends Comparable<T>> extends PreflowPush<T> {
	private final ArrayList<ReducedNetworkVertex<T>> activeVertices = new ArrayList<ReducedNetworkVertex<T>>();
	private final Random random = new Random();

	/**
	 * Creates a new RandomPreflowPush instance with a given Flow Network.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 * @param log
	 *            Stream to print log to, or null to print no log
	 */
	public RandomVertexPreflowPush(FlowNetwork<T> flowNetwork, PrintStream log) {
		super(flowNetwork, log);
	}

	protected void addActiveVertex(ReducedNetworkVertex<T> vertex) {
		activeVertices.add(vertex);
	}

	protected void removePushedVertexFromActive(ReducedNetworkVertex<T> vertex) {
		activeVertices.remove(vertex);
	}

	public void startPreflowPush() {
		for (FlowEdge<T> sourceEdge : getFlowNetwork().getSource()) {
			@SuppressWarnings("unchecked")
			FlowVertex<T> end = (FlowVertex<T>) sourceEdge.getEnd();
			if (end.getAttributeValue(FlowUtilities.EXCESS) > 0) {
				activeVertices.add(end.getReducedVertex());
			}
		}

		while (activeVertices.size() > 0) {
			ReducedNetworkVertex<T> active = activeVertices.get(random
					.nextInt(activeVertices.size()));

			testPushOrLift(active);
		}
	}
}
