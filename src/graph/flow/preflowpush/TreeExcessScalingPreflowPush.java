package graph.flow.preflowpush;

import graph.attribute.Attribute;
import graph.attribute.DefaultAttribute;
import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.FlowVertex;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.LinkedList;
import java.util.TreeSet;

/**
 * Preflow-Push algorithm using the Excess Scaling algorithm. This version uses
 * a binary tree to store the highest excess edges.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class TreeExcessScalingPreflowPush<T extends Comparable<T>> extends
		PreflowPush<T> {
	private final Attribute<ReducedNetworkEdge<T>> highestExcessEdgeAttribute = new DefaultAttribute<ReducedNetworkEdge<T>>(
			"Hightest Excess Edge", null);
	private final Attribute<Double> highestExcessEdgeExcessAttribute = new DefaultAttribute<Double>(
			"Hightest Excess Edge Excess", 0.);

	private final LinkedList<ReducedNetworkVertex<T>> liftableVertices = new LinkedList<ReducedNetworkVertex<T>>();
	private final TreeSet<ReducedNetworkVertex<T>> pushableEdges;
	private boolean wasSaturating = false;
	private int maxPushableEdges;

	/**
	 * Creates a new RandomPreflowPush instance with a given Flow Network.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 * @param log
	 *            Stream to print log to, or null to print no log
	 */
	public TreeExcessScalingPreflowPush(FlowNetwork<T> flowNetwork,
			PrintStream log) {
		super(flowNetwork, log);
		pushableEdges = new TreeSet<ReducedNetworkVertex<T>>(
				new Comparator<ReducedNetworkVertex<T>>() {
					@Override
					public int compare(ReducedNetworkVertex<T> o1,
							ReducedNetworkVertex<T> o2) {
						Double o1MaxExcess = o1
								.getAttributeValue(highestExcessEdgeExcessAttribute);
						Double o2MaxExcess = o2
								.getAttributeValue(highestExcessEdgeExcessAttribute);
						int comparison = o1MaxExcess.compareTo(o2MaxExcess);
						return comparison == 0 ? o1.compareTo(o2) : comparison;
					}
				});
	}

	protected void addActiveVertex(ReducedNetworkVertex<T> vertex) {
		checkForGreatestEdge(vertex);
	}

	private void checkForGreatestEdge(ReducedNetworkVertex<T> vertex) {
		int height = vertex.getAttributeValue(FlowUtilities.HEIGHT);
		ReducedNetworkEdge<T> greatestEdge = null;
		double max = -Double.MAX_VALUE;
		for (ReducedNetworkEdge<T> edge : vertex) {
			if (edge.getRemainingCapacity() == 0) {
				continue;
			}
			int heightTarget = edge.getEnd().getAttributeValue(
					FlowUtilities.HEIGHT);
			if (heightTarget < height) {
				double diff = vertex.getAttributeValue(FlowUtilities.EXCESS)
						- edge.getEnd().getAttributeValue(FlowUtilities.EXCESS);
				if (max < diff) {
					max = diff;
					greatestEdge = edge;
				}
			}
		}
		if (greatestEdge != null) {
			pushableEdges.remove(vertex);
			vertex.setAttributeValue(highestExcessEdgeAttribute, greatestEdge);
			vertex.setAttributeValue(highestExcessEdgeExcessAttribute, max);
			pushableEdges.add(vertex);
			if (pushableEdges.size() > maxPushableEdges) {
				maxPushableEdges = pushableEdges.size();
			}
		} else {
			removePushableVertex(vertex);
			liftableVertices.add(vertex);
		}
	}

	private void removePushableVertex(ReducedNetworkVertex<T> vertex) {
		pushableEdges.remove(vertex);
		vertex.setAttributeValue(highestExcessEdgeAttribute, null);
		vertex.setAttributeValue(highestExcessEdgeExcessAttribute, 0.);
	}

	protected void removePushedVertexFromActive(ReducedNetworkVertex<T> vertex) {
		removePushableVertex(vertex);
		wasSaturating = true;
	}

	@SuppressWarnings("unchecked")
	public void startPreflowPush() {
		for (FlowEdge<T> sourceEdge : getFlowNetwork().getSource()) {
			FlowVertex<T> end = (FlowVertex<T>) sourceEdge.getEnd();
			if (end.getAttributeValue(FlowUtilities.EXCESS) > 0) {
				liftableVertices.add(end.getReducedVertex());
			}
		}

		while (true) {
			// lift every liftable vertex
			for (ReducedNetworkVertex<T> lifted : liftableVertices) {
				// lift
				lift(lifted);
				checkForGreatestEdge(lifted);
			}
			liftableVertices.clear();

			while (pushableEdges.size() > 0) {
				ReducedNetworkEdge<T> maxEdge = pushableEdges.last()
						.getAttributeValue(highestExcessEdgeAttribute);

				wasSaturating = false;
				push(maxEdge);
				if (!wasSaturating) {
					// Update greatest edge for start vertex
					checkForGreatestEdge((ReducedNetworkVertex<T>) maxEdge
							.getStart());
				}

				if (liftableVertices.size() > 0) {
					break;
				} else if (pushableEdges.size() == 0) {
					return;
				}
			}

		}
	}
}
