package graph.flow.preflowpush;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.FlowVertex;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;

import java.io.PrintStream;
import java.util.LinkedList;

/**
 * Preflow-Push algorithm using the FIFO algorithm.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class FIFOPreflowPush<T extends Comparable<T>> extends PreflowPush<T> {

	private final LinkedList<ReducedNetworkVertex<T>> activeVertices = new LinkedList<ReducedNetworkVertex<T>>();

	/**
	 * Creates a new RandomPreflowPush instance with a given Flow Network.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 * @param log
	 *            Stream to print log to, or null to print no log
	 */
	public FIFOPreflowPush(FlowNetwork<T> flowNetwork, PrintStream log) {
		super(flowNetwork, log);
	}

	protected void addActiveVertex(ReducedNetworkVertex<T> vertex) {
		activeVertices.add(vertex);
	}

	protected void removePushedVertexFromActive(ReducedNetworkVertex<T> vertex) {
		activeVertices.remove(vertex);
	}

	protected void examine(ReducedNetworkVertex<T> active) {
		int height = active.getAttributeValue(FlowUtilities.HEIGHT);
		for (ReducedNetworkEdge<T> edge : active) {
			if (edge.getRemainingCapacity() == 0) {
				// Edge does not really exist
				continue;
			}
			int otherHeight = edge.getEnd().getAttributeValue(
					FlowUtilities.HEIGHT);
			if (height > otherHeight) {
				if (push(edge)) {
					// saturating push, no further push possible
					return;
				}
			}
		}

		lift(active);
		// Lift and move to the end of the FIFO queue
		activeVertices.add(activeVertices.removeFirst());
	}

	public void startPreflowPush() {
		for (FlowEdge<T> sourceEdge : getFlowNetwork().getSource()) {
			@SuppressWarnings("unchecked")
			FlowVertex<T> end = (FlowVertex<T>) sourceEdge.getEnd();
			if (end.getAttributeValue(FlowUtilities.EXCESS) > 0) {
				activeVertices.add(end.getReducedVertex());
			}
		}

		while (activeVertices.size() > 0) {
			ReducedNetworkVertex<T> active = activeVertices.peekFirst();

			examine(active);
		}
	}
}
