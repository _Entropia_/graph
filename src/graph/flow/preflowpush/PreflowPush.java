package graph.flow.preflowpush;

import java.io.PrintStream;

import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;

/**
 * Abstract base class for Preflow Push Algorithms.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public abstract class PreflowPush<T extends Comparable<T>> {
	private final FlowNetwork<T> flowNetwork;
	private int nPushs, nLifts;
	private final PrintStream log;

	protected PreflowPush(FlowNetwork<T> flowNetwork, PrintStream log) {
		this.flowNetwork = flowNetwork;
		this.log = log;
	}

	protected FlowNetwork<T> getFlowNetwork() {
		return flowNetwork;
	}

	/**
	 * Tests whether push or lift can be applied to this vertex and performs the
	 * operation.
	 * 
	 * @param active
	 *            Active vertex
	 */
	protected void testPushOrLift(ReducedNetworkVertex<T> active) {
		int height = active.getAttributeValue(FlowUtilities.HEIGHT);
		for (ReducedNetworkEdge<T> edge : active) {
			if (edge.getRemainingCapacity() == 0) {
				// Edge does not really exist
				continue;
			}
			int otherHeight = edge.getEnd().getAttributeValue(
					FlowUtilities.HEIGHT);
			if (height > otherHeight) {
				push(edge);
				return;
			}
		}

		lift(active);
	}

	/**
	 * Lifts an active vertex.
	 * 
	 * @param activeVertex
	 *            Active vertex
	 */
	protected void lift(ReducedNetworkVertex<T> activeVertex) {
		if (activeVertex.getEdgeCount() == 0) {
			throw new RuntimeException(
					"Cannot lift vertex without outgoing edges.");
		}
		int minHeight = Integer.MAX_VALUE;
		for (ReducedNetworkEdge<T> edge : activeVertex) {
			if (edge.getRemainingCapacity() == 0) {
				// Edge does not really exist
				continue;
			}
			int height = edge.getEnd().getAttributeValue(FlowUtilities.HEIGHT);
			if (height < minHeight) {
				minHeight = height;
			}
		}
		log.println("Lift " + activeVertex + " to level "
				+ (minHeight + 1));
		activeVertex.setAttributeValue(FlowUtilities.HEIGHT, minHeight + 1);
		++nLifts;
	}

	/**
	 * Pushes flow over an edge whose start is an active vertex.
	 * 
	 * @param edge
	 *            Edge
	 * @return true, if the push was saturating, false otherwise
	 */
	@SuppressWarnings("unchecked")
	protected boolean push(ReducedNetworkEdge<T> edge) {
		Double startExcess = edge.getStart().getAttributeValue(
				FlowUtilities.EXCESS);
		Double targetExcess = edge.getEnd().getAttributeValue(
				FlowUtilities.EXCESS);
		boolean addTargetToActive = targetExcess == 0
				&& edge.getEnd() != flowNetwork.getTarget().getReducedVertex();
		double delta = Math.min(startExcess, edge.getRemainingCapacity());
		log.println("Push " + delta + " over " + edge);
		edge.updateFlow(delta);
		edge.getStart().setAttributeValue(FlowUtilities.EXCESS,
				startExcess - delta);
		edge.getEnd().setAttributeValue(FlowUtilities.EXCESS,
				targetExcess + delta);

		++nPushs;
		if (addTargetToActive) {
			addActiveVertex((ReducedNetworkVertex<T>) edge.getEnd());
		}
		if (startExcess - delta == 0) {
			removePushedVertexFromActive((ReducedNetworkVertex<T>) edge
					.getStart());
			return true;
		} else {
			return false;
		}
	}

	/**
	 * @return After running the algorithm, the number of lift operations
	 *         performed to calculate the maximum flow
	 */
	public int getLiftCount() {
		return nLifts;
	}

	/**
	 * @return After running the algorithm, the number of push operations
	 *         performed to calculate the maximum flow
	 */
	public int getPushCount() {
		return nPushs;
	}

	/**
	 * Initiates the Preflow-Push-Algorithm.
	 */
	public abstract void startPreflowPush();

	protected abstract void addActiveVertex(ReducedNetworkVertex<T> newActive);

	protected abstract void removePushedVertexFromActive(
			ReducedNetworkVertex<T> newActive);

}