package graph.flow.preflowpush;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.FlowVertex;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;

import java.io.PrintStream;
import java.util.Comparator;
import java.util.PriorityQueue;

/**
 * Preflow-Push algorithm using the Highest-Label algorithm. The vertex with the
 * greatest height is pushed.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class HighestLabelPreflowPush<T extends Comparable<T>> extends
		PreflowPush<T> {

	private final PriorityQueue<ReducedNetworkVertex<T>> activeVertices;

	/**
	 * Creates a new RandomPreflowPush instance with a given Flow Network.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 * @param log
	 *            Stream to print log to, or null to print no log
	 */
	public HighestLabelPreflowPush(FlowNetwork<T> flowNetwork, PrintStream log) {
		super(flowNetwork, log);
		activeVertices = new PriorityQueue<ReducedNetworkVertex<T>>(
				flowNetwork.getVertexCount(),
				new Comparator<ReducedNetworkVertex<T>>() {
					@Override
					public int compare(ReducedNetworkVertex<T> o1,
							ReducedNetworkVertex<T> o2) {
						return o2
								.getAttributeValue(FlowUtilities.HEIGHT)
								.compareTo(
										o1.getAttributeValue(FlowUtilities.HEIGHT));
					}
				});
	}

	protected void addActiveVertex(ReducedNetworkVertex<T> vertex) {
		activeVertices.add(vertex);
	}

	protected void removePushedVertexFromActive(ReducedNetworkVertex<T> vertex) {
		activeVertices.remove(vertex);
	}

	protected void examine(ReducedNetworkVertex<T> active) {
		int height = active.getAttributeValue(FlowUtilities.HEIGHT);
		for (ReducedNetworkEdge<T> edge : active) {
			if (edge.getRemainingCapacity() == 0) {
				// Edge does not really exist
				continue;
			}
			int otherHeight = edge.getEnd().getAttributeValue(
					FlowUtilities.HEIGHT);
			if (height > otherHeight) {
				if (push(edge)) {
					// saturating push, no further push possible
					return;
				}
			}
		}

		lift(active);
	}

	public void startPreflowPush() {
		for (FlowEdge<T> sourceEdge : getFlowNetwork().getSource()) {
			@SuppressWarnings("unchecked")
			FlowVertex<T> end = (FlowVertex<T>) sourceEdge.getEnd();
			if (end.getAttributeValue(FlowUtilities.EXCESS) > 0) {
				activeVertices.add(end.getReducedVertex());
			}
		}

		while (activeVertices.size() > 0) {
			ReducedNetworkVertex<T> active = activeVertices.peek();

			examine(active);
		}
	}
}
