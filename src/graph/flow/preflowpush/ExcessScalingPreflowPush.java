package graph.flow.preflowpush;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.FlowVertex;
import graph.flow.ReducedNetworkEdge;
import graph.flow.ReducedNetworkVertex;

import java.io.PrintStream;
import java.util.HashMap;
import java.util.LinkedList;

/**
 * Preflow-Push algorithm using the Excess Scaling algorithm. This version uses
 * a hash map to store the highest excess edges, therefore the number of push-
 * and lift-operations is non-deterministic.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class ExcessScalingPreflowPush<T extends Comparable<T>> extends
		PreflowPush<T> {

	private final LinkedList<ReducedNetworkVertex<T>> liftableVertices = new LinkedList<ReducedNetworkVertex<T>>();
	private final HashMap<ReducedNetworkVertex<T>, ReducedNetworkEdge<T>> pushableEdges;
	private boolean wasSaturating = false;
	private int maxPushableEdges;

	/**
	 * Creates a new RandomPreflowPush instance with a given Flow Network.
	 * 
	 * @param flowNetwork
	 *            Flow Network
	 * @param log
	 *            Stream to print log to, or null to print no log
	 */
	public ExcessScalingPreflowPush(FlowNetwork<T> flowNetwork, PrintStream log) {
		super(flowNetwork, log);
		pushableEdges = new HashMap<ReducedNetworkVertex<T>, ReducedNetworkEdge<T>>(
				flowNetwork.getVertexCount());
	}

	protected void addActiveVertex(ReducedNetworkVertex<T> vertex) {
		checkForGreatestEdge(vertex);
	}

	private void checkForGreatestEdge(ReducedNetworkVertex<T> vertex) {
		int height = vertex.getAttributeValue(FlowUtilities.HEIGHT);
		ReducedNetworkEdge<T> greatestEdge = null;
		double max = -Double.MAX_VALUE;
		for (ReducedNetworkEdge<T> edge : vertex) {
			if (edge.getRemainingCapacity() == 0) {
				continue;
			}
			int heightTarget = edge.getEnd().getAttributeValue(
					FlowUtilities.HEIGHT);
			if (heightTarget < height) {
				double diff = vertex.getAttributeValue(FlowUtilities.EXCESS)
						- edge.getEnd().getAttributeValue(FlowUtilities.EXCESS);
				if (max < diff) {
					max = diff;
					greatestEdge = edge;
				}
			}
		}
		if (greatestEdge != null) {
			pushableEdges.put(vertex, greatestEdge);
			if (pushableEdges.size() > maxPushableEdges) {
				maxPushableEdges = pushableEdges.size();
			}
		} else {
			pushableEdges.remove(vertex);
			liftableVertices.add(vertex);
		}
	}

	protected void removePushedVertexFromActive(ReducedNetworkVertex<T> vertex) {
		pushableEdges.remove(vertex);
		wasSaturating = true;
	}

	@SuppressWarnings("unchecked")
	public void startPreflowPush() {
		for (FlowEdge<T> sourceEdge : getFlowNetwork().getSource()) {
			FlowVertex<T> end = (FlowVertex<T>) sourceEdge.getEnd();
			if (end.getAttributeValue(FlowUtilities.EXCESS) > 0) {
				liftableVertices.add(end.getReducedVertex());
			}
		}

		while (true) {
			// lift every liftable vertex
			for (ReducedNetworkVertex<T> lifted : liftableVertices) {
				// lift
				lift(lifted);
				checkForGreatestEdge(lifted);
			}
			liftableVertices.clear();

			while (pushableEdges.size() > 0) {
				ReducedNetworkEdge<T> maxEdge = null;
				double max = -Double.MAX_VALUE;
				for (ReducedNetworkEdge<T> edge : pushableEdges.values()) {
					double excess = edge.getStart().getAttributeValue(
							FlowUtilities.EXCESS)
							- edge.getEnd().getAttributeValue(
									FlowUtilities.EXCESS);

					if (excess > max) {
						max = excess;
						maxEdge = edge;
					}
				}

				wasSaturating = false;
				push(maxEdge);
				if (!wasSaturating) {
					// Update greatest edge for start vertex
					checkForGreatestEdge((ReducedNetworkVertex<T>) maxEdge
							.getStart());
				}

				if (liftableVertices.size() > 0) {
					break;
				} else if (pushableEdges.size() == 0) {
					return;
				}
			}

		}
	}
}
