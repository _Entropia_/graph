package graph.flow;

import graph.Vertex;
import graph.defaultgraph.DefaultEdge;

/**
 * An Edge in a Flow Network.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 */
public class FlowEdge<T extends Comparable<T>> extends DefaultEdge<T> {
	private double flow;
	private FlowEdge<T> backwardEdge;
	private ReducedNetworkEdge<T> reducedEdge;

	FlowEdge(Vertex<T, FlowEdge<T>> start, Vertex<T, FlowEdge<T>> end,
			double weight, double flow) {
		super(start, end, weight);
		this.flow = flow;
	}

	/**
	 * @return The flow through this edge
	 */
	public double getFlow() {
		return flow;
	}

	/**
	 * Sets the flow through this edge.
	 * 
	 * @param flow
	 *            New flow
	 */
	public void setFlow(double flow) {
		if (flow > getCapacity()) {
			throw new IllegalArgumentException(
					"Flow cannot be greater than the capacity of an edge.");
		}
		this.flow = flow;
	}

	/**
	 * @return The corresponding backward edge
	 */
	public FlowEdge<T> getBackwardEdge() {
		return backwardEdge;
	}

	void setBackwardEdge(FlowEdge<T> backwardEdge) {
		this.backwardEdge = backwardEdge;
	}

	/**
	 * @return The edge in the corresponding Reduced Network.
	 */
	public ReducedNetworkEdge<T> getReducedEdge() {
		if (reducedEdge == null) {
			reducedEdge = new ReducedNetworkEdge<T>(this);
		}
		return reducedEdge;
	}

	/**
	 * @return Capacity of this edge. Equal to {@link #getWeight()}.
	 * @see #getWeight()
	 */
	public double getCapacity() {
		return super.getWeight();
	}
}
