package graph.utils;

import graph.Edge;
import graph.Vertex;

/**
 * Adapter class for DFSListener.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 * @param <VERTEX>
 *            Vertex type
 */
public abstract class GraphSearchAdapter<T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>>
		implements GraphSearchListener<T, EDGE, VERTEX> {

	public void blackVertexDetected(VERTEX current, VERTEX blackVertex,
			EDGE edge) {
	};

	public void grayVertexDetected(VERTEX current, VERTEX grayVertex, EDGE edge) {
	};

	public boolean vertexFinished(VERTEX vertex) {
		return true;
	};

	public boolean vertexVisited(VERTEX vertex) {
		return true;
	};

	public boolean whiteVertexDetected(VERTEX current, VERTEX whiteVertex,
			EDGE edge) {
		return true;
	};

}
