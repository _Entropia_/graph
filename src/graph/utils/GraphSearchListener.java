package graph.utils;

import graph.Edge;
import graph.Vertex;

/**
 * Listens to events during a Depth First Search and manipulates the search.
 * 
 * @author Corny
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 * @param <VERTEX>
 *            Vertex type
 */
public interface GraphSearchListener<T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>> {
	/**
	 * Called when a new (white) vertex is visited.
	 * 
	 * @param vertex
	 *            Vertex to visit
	 * @return false to cancel search at this point
	 */
	boolean vertexVisited(VERTEX vertex);

	/**
	 * Called when a vertex is finished and about to turn black.
	 * 
	 * @param vertex
	 *            Finished Vertex
	 * @return false to cancel search at this point
	 */
	boolean vertexFinished(VERTEX vertex);

	/**
	 * Called whenever a white vertex is detected (i.e., directly reachable)
	 * from a vertex which is currently visited.
	 * 
	 * @param current
	 *            Currently visited vertex
	 * @param whiteVertex
	 *            Detected white vertex
	 * @param edge
	 *            Edge
	 * @return false if the vertex should not be visited. Default is true.
	 */
	boolean whiteVertexDetected(VERTEX current, VERTEX whiteVertex, EDGE edge);

	/**
	 * Called whenever a gray vertex is detected (i.e., directly reachable) from
	 * a vertex which is currently visited.
	 * 
	 * @param current
	 *            Currently visited vertex
	 * @param grayVertex
	 *            Detected gray vertex
	 * @param edge
	 *            Edge
	 */
	void grayVertexDetected(VERTEX current, VERTEX grayVertex, EDGE edge);

	/**
	 * Called whenever a black vertex is detected (i.e., directly reachable)
	 * from a vertex which is currently visited.
	 * 
	 * @param current
	 *            Currently visited vertex
	 * @param blackVertex
	 *            Detected white vertex
	 * @param edge
	 *            Edge
	 */
	void blackVertexDetected(VERTEX current, VERTEX blackVertex, EDGE edge);
}
