package graph.utils;

import graph.Edge;
import graph.Vertex;

import java.util.Iterator;
import java.util.NoSuchElementException;

/**
 * Iterator which iterates edges using the "previous" attribute of vertices.
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 * @param <VERTEX>
 *            Vertex type
 */
public class PreviousVertexIterator<T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>>
		implements Iterator<EDGE> {
	private EDGE nextVertex;

	/**
	 * Creates a new iterator, starting at a given vertex and iterating over the
	 * "previous" edges.
	 * 
	 * @param startVertex
	 */
	@SuppressWarnings("unchecked")
	public PreviousVertexIterator(VERTEX startVertex) {
		nextVertex = (EDGE) startVertex
				.getAttributeValue(GraphUtilities.PREVIOUS_EDGE);
	}

	@Override
	public boolean hasNext() {
		return nextVertex != null;
	}

	@SuppressWarnings("unchecked")
	@Override
	public EDGE next() {
		if (nextVertex == null) {
			throw new NoSuchElementException("No previous vertex.");
		}
		EDGE former = nextVertex;

		nextVertex = (EDGE) nextVertex.getStart().getAttributeValue(
				GraphUtilities.PREVIOUS_EDGE);

		return former;
	}

	@Override
	public void remove() {
		throw new UnsupportedOperationException(
				"Cannot remove edges from graph.");
	}

}
