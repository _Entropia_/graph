package graph.utils;

import graph.Edge;
import graph.Graph;
import graph.Vertex;
import graph.attribute.Attribute;
import graph.attribute.DefaultAttribute;

import java.util.Iterator;
import java.util.LinkedList;
import java.util.ListIterator;

/**
 * Utilities for Graph operations.
 * 
 * @author Corny
 * 
 */
public class GraphUtilities {
	/**
	 * Possible vertex colors
	 * 
	 * @author Corny
	 */
	public enum VertexColor {
		/**
		 * White color (unvisited)
		 */
		WHITE,
		/**
		 * Gray color (visited, not finished)
		 */
		GRAY,
		/**
		 * Black color (finished)
		 */
		BLACK;
	}

	/**
	 * Color attribute
	 */
	public static final Attribute<VertexColor> COLOR = new DefaultAttribute<GraphUtilities.VertexColor>(
			"Color", VertexColor.WHITE);
	/**
	 * Distance attribute in BFS, visit time in DFS
	 */
	public static final Attribute<Integer> DISTANCE = new DefaultAttribute<Integer>(
			"Distance", Integer.MAX_VALUE);
	/**
	 * Finished time, used in depth first search
	 */
	public static final Attribute<Integer> FINISHED = new DefaultAttribute<Integer>(
			"Finished", Integer.MAX_VALUE);
	/**
	 * Previous Vertex attribute
	 */
	public static final Attribute<Vertex<?, ?>> PREVIOUS = new DefaultAttribute<Vertex<?, ?>>(
			"Previous", null);
	/**
	 * Previous Vertex attribute
	 */
	public static final Attribute<Edge<?>> PREVIOUS_EDGE = new DefaultAttribute<Edge<?>>(
			"Edge to Previous", null);

	/**
	 * performs a breadth first search
	 * 
	 * @param g
	 *            Graph
	 * @param startVertex
	 *            Start vertex
	 * @param l
	 *            BFS Listener
	 */
	@SuppressWarnings("unchecked")
	public static <T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>> void breadthFirstSearch(
			Graph<T, EDGE, VERTEX> g, VERTEX startVertex,
			GraphSearchListener<T, EDGE, VERTEX> l) {
		for (VERTEX vertex : g) {
			vertex.setAttributeValue(DISTANCE, Integer.MAX_VALUE);
			vertex.setAttributeValue(PREVIOUS, null);
			vertex.setAttributeValue(PREVIOUS_EDGE, null);
			vertex.setAttributeValue(COLOR, VertexColor.WHITE);
		}

		startVertex.setAttributeValue(DISTANCE, 0);

		LinkedList<VERTEX> remaining = new LinkedList<VERTEX>();
		remaining.add(startVertex);

		while (remaining.size() > 0) {
			VERTEX current = remaining.pop();
			if (l != null) {
				if (!l.vertexVisited(current)) {
					break;
				}
			}

			for (Edge<T> edge : current) {
				int currentDistance = current.getAttributeValue(DISTANCE);
				VERTEX other = (VERTEX) edge.getEnd();
				switch (other.getAttributeValue(COLOR)) {
				case WHITE:
					if (l != null) {
						if (!l.whiteVertexDetected(current, other, (EDGE) edge)) {
							break;
						}
					}
					other.setAttributeValue(COLOR, VertexColor.GRAY);
					other.setAttributeValue(DISTANCE, currentDistance + 1);
					other.setAttributeValue(PREVIOUS, current);
					other.setAttributeValue(PREVIOUS_EDGE, edge);

					remaining.add(other);
					break;
				case GRAY:
					if (l != null) {
						l.grayVertexDetected(current, other, (EDGE) edge);
					}
					break;
				case BLACK:
					if (l != null) {
						l.blackVertexDetected(current, other, (EDGE) edge);
					}
					break;
				}
			}

			current.setAttributeValue(COLOR, VertexColor.BLACK);
			if (l != null) {
				if (!l.vertexFinished(current)) {
					break;
				}
			}
		}
	}

	/**
	 * Performs a depth first search with a given start vertex.
	 * 
	 * @param g
	 *            Graph
	 * @param startVertex
	 *            Vertex to start search from
	 * @param onlyPathsFromStart
	 *            If true, search will terminate when the all vertices reachable
	 *            from startVertex are finished.
	 * @param l
	 *            DFS listener
	 */
	@SuppressWarnings("unchecked")
	public static <T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>> void depthFirstSearch(
			Graph<T, EDGE, VERTEX> g, VERTEX startVertex,
			boolean onlyPathsFromStart, GraphSearchListener<T, EDGE, VERTEX> l) {
		for (VERTEX vertex : g) {
			vertex.setAttributeValue(PREVIOUS, null);
			vertex.setAttributeValue(PREVIOUS_EDGE, null);
			vertex.setAttributeValue(COLOR, VertexColor.WHITE);
		}

		int time = 0;

		Iterator<VERTEX> graphIterator = g.iterator();

		LinkedList<VERTEX> stack = new LinkedList<VERTEX>();
		stack.push(startVertex);

		while (stack.size() > 0) {
			VERTEX current = stack.peek();

			if (current.getAttributeValue(COLOR) == VertexColor.GRAY) {
				// Only white vertices are pushed to stack. If it is gray, is
				// must be finished.
				current.setAttributeValue(COLOR, VertexColor.BLACK);
				current.setAttributeValue(FINISHED, ++time);
				stack.pop();

				if (l != null) {
					if (!l.vertexFinished(current)) {
						// Should cancel search
						break;
					}
				}

				// Removed Vertex from Stack, ensure search does not finish
				// prematurely
				if (stack.size() == 0) {
					while (!onlyPathsFromStart && graphIterator.hasNext()) {
						VERTEX nextInGraph = graphIterator.next();
						if (nextInGraph.getAttributeValue(COLOR) == VertexColor.WHITE) {
							stack.push(nextInGraph);
							break;
						}
					}
				}
				continue;
			}

			current.setAttributeValue(COLOR, VertexColor.GRAY);
			current.setAttributeValue(DISTANCE, ++time);

			if (l != null) {
				if (!l.vertexVisited(current)) {
					// Should cancel search
					break;
				}
			}

			// Preserve order of edges
			ListIterator<EDGE> edgeIt = current.listIterator(current
					.getEdgeCount());
			while (edgeIt.hasPrevious()) {
				Edge<T> edge = edgeIt.previous();
				VERTEX other = (VERTEX) edge.getEnd();
				VertexColor otherColor = other.getAttributeValue(COLOR);
				switch (otherColor) {
				case WHITE:
					if (l != null) {
						if (!l.whiteVertexDetected(current, other, (EDGE) edge)) {
							break;
						}
					}
					other.setAttributeValue(PREVIOUS, current);
					other.setAttributeValue(PREVIOUS_EDGE, edge);
					stack.push(other);
					break;
				case GRAY:
					if (l != null) {
						l.grayVertexDetected(current, other, (EDGE) edge);
					}
					break;
				case BLACK:
					if (l != null) {
						l.blackVertexDetected(current, other, (EDGE) edge);
					}
					break;
				}
			}
		}
	}

	/**
	 * Performs a depth first search on a given graph.
	 * 
	 * @param g
	 *            Graph
	 * @param l
	 *            DFS listener
	 */
	public static <T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>> void depthFirstSearch(
			Graph<T, EDGE, VERTEX> g, GraphSearchListener<T, EDGE, VERTEX> l) {
		Iterator<VERTEX> it = g.iterator();
		if (!it.hasNext()) {
			return;
		}
		depthFirstSearch(g, it.next(), false, l);
	}
}
