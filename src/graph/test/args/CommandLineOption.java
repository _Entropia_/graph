package graph.test.args;

import java.io.File;
import java.io.FileNotFoundException;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

/**
 * Command Line Argument
 * 
 * @author Corny
 * @param <T>
 *            Type of value this argument can contain
 * 
 */
public interface CommandLineOption<T> {
	/**
	 * Returns the value of this Argument.
	 * 
	 * @param option
	 *            Option
	 * @return Argument value
	 */
	T getValue();

	/**
	 * Hides this argument.
	 * 
	 * @return this
	 */
	CommandLineOption<T> hide();
}

abstract class CommandLineOptionImpl<T> implements CommandLineOption<T> {
	private final String description;
	private final String valDescription;
	private final String longRepresentation;
	private final Character shortRepresentation;
	private boolean hidden;

	final T defaultValue;
	protected T value;

	public CommandLineOptionImpl(Character shortRepresentation,
			String longRepresentation, String desc, String valDesc,
			T defaultValue) {
		this.shortRepresentation = shortRepresentation;
		this.longRepresentation = longRepresentation;

		desc = desc.trim();
		if (desc.endsWith(".")) {
			desc = desc.substring(0, desc.length() - 1);
		}
		this.description = desc;
		this.valDescription = valDesc;
		this.defaultValue = defaultValue;
	}

	@Override
	public T getValue() {
		return value != null ? value : defaultValue;
	}

	String getDescription() {
		return description;
	}

	String getValueDescription() {
		return valDescription;
	}

	String getLongRepresentation() {
		return longRepresentation;
	}

	Character getShortRepresentation() {
		return shortRepresentation;
	}

	@Override
	public CommandLineOption<T> hide() {
		hidden = true;
		return this;
	}

	boolean isHidden() {
		return hidden;
	}

	protected void createHelpString(StringBuilder build) {
	}

	protected abstract boolean needsValue();

	protected abstract void setValue(String valueRep) throws Exception;

	/**
	 * Creates and writes to the StringBuilder the String which tells bash
	 * completion how to generate completions.
	 * 
	 * @param build
	 *            StringBuilder
	 */
	protected void createBashCompletionCase(StringBuilder build) {
		build.append("COMPREPLY=()");
	}
}

final class BooleanOption extends CommandLineOptionImpl<Boolean> {
	public BooleanOption(Character shortRepresentation,
			String longRepresentation, String desc) {
		super(shortRepresentation, longRepresentation, desc, null, false);
	}

	@Override
	public boolean needsValue() {
		return false;
	}

	@Override
	protected void setValue(String valueRep) {
		value = true;
	}

	@Override
	public Boolean getValue() {
		return (value == null ? false : value);
	}
}

final class IntegerOption extends CommandLineOptionImpl<Integer> {
	public IntegerOption(Character shortRepresentation,
			String longRepresentation, String desc, String valDesc,
			Integer defaultValue) {
		super(shortRepresentation, longRepresentation, desc, valDesc,
				defaultValue);
	}

	@Override
	public boolean needsValue() {
		return true;
	}

	@Override
	protected void setValue(String valueRep) throws NumberFormatException {
		value = Integer.valueOf(valueRep);
	}
}

final class DoubleOption extends CommandLineOptionImpl<Double> {
	public DoubleOption(Character shortRepresentation,
			String longRepresentation, String desc, String valDesc,
			Double defaultValue) {
		super(shortRepresentation, longRepresentation, desc, valDesc,
				defaultValue);
	}

	@Override
	public boolean needsValue() {
		return true;
	}

	@Override
	protected void setValue(String valueRep) throws NumberFormatException {
		value = Double.valueOf(valueRep);
	}
}

final class StringOption extends CommandLineOptionImpl<String> {
	public StringOption(Character shortRepresentation,
			String longRepresentation, String desc, String valDesc,
			String defaultValue) {
		super(shortRepresentation, longRepresentation, desc, valDesc,
				defaultValue);
	}

	@Override
	public boolean needsValue() {
		return true;
	}

	@Override
	protected void setValue(String valueRep) {
		value = valueRep;
	}
}

final class FileOption extends CommandLineOptionImpl<File> {
	private final boolean ensureFileExists;
	private final boolean isDirectory;

	public FileOption(Character shortRepresentation,
			String longRepresentation, String desc, String valDesc,
			File defaultValue, boolean ensureFileExists, boolean isDirectory) {
		super(shortRepresentation, longRepresentation, desc, valDesc,
				defaultValue);
		this.ensureFileExists = ensureFileExists;
		this.isDirectory = isDirectory;
	}

	@Override
	public boolean needsValue() {
		return true;
	}

	@Override
	protected void setValue(String valueRep) throws FileNotFoundException {
		File f = new File(valueRep);
		if (ensureFileExists) {
			if (!f.exists()) {
				throw new FileNotFoundException("File " + valueRep
						+ " not found.");
			} else if (f.isDirectory() != isDirectory) {
				throw new FileNotFoundException("Path " + valueRep
						+ " does not specify a "
						+ (isDirectory ? "directory." : "file."));
			}
		}
		value = f;
	}

	@Override
	protected void createBashCompletionCase(StringBuilder build) {
		build.append("_filedir_xspec \"${cur}\"");
	}
}

final class ChoiceOption<T> extends CommandLineOptionImpl<T> {
	private final List<String> possibilities;
	private Map<String, T> repToObject;

	public ChoiceOption(Character shortRepresentation,
			String longRepresentation, String desc, String valDesc,
			T[] possibilities, T defaultValue) {
		super(shortRepresentation, longRepresentation, desc, valDesc,
				defaultValue);
		this.possibilities = new ArrayList<String>(possibilities.length);
		this.repToObject = new HashMap<String, T>(possibilities.length);
		for (T poss : possibilities) {
			String rep = poss.toString();
			this.possibilities.add(rep);
			this.repToObject.put(rep.toLowerCase(), poss);
		}
	}

	@Override
	public boolean needsValue() {
		return true;
	}

	List<String> getPossibilities() {
		return possibilities;
	}

	protected void createHelpString(StringBuilder builder) {
		builder.append("Possible values for \"");
		builder.append(getValueDescription());
		builder.append("\": ");
		boolean first = true;
		for (String s : this.possibilities) {
			if (first) {
				first = false;
			} else {
				builder.append(", ");
			}
			builder.append('"');
			builder.append(s);
			builder.append('"');
		}
	}

	@Override
	protected void setValue(String valueRep) throws IllegalArgumentException {
		String valueRepLower = valueRep.toLowerCase();
		valueRep = valueRepLower;
		if (this.repToObject.containsKey(valueRepLower)) {
			value = repToObject.get(valueRepLower);
		} else {
			StringBuilder builder = new StringBuilder(
					"Invalid value for argument \"");
			builder.append(getLongRepresentation());
			builder.append("\". Possible values:");
			for (String s : this.possibilities) {
				builder.append(" \"");
				builder.append(s);
				builder.append("\"");
			}
			throw new IllegalArgumentException(builder.toString());
		}
	}

	@Override
	protected void createBashCompletionCase(StringBuilder build) {
		build.append("COMPREPLY=($(compgen -W \"");
		boolean first = true;
		for (String poss : getPossibilities()) {
			if (first) {
				first = false;
			} else {
				build.append(' ');
			}
			build.append(poss);
		}
		build.append("\" -- ${cur}))");
	}
}