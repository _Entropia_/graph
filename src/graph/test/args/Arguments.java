package graph.test.args;

import java.io.ByteArrayOutputStream;
import java.io.File;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.OutputStream;
import java.io.PrintStream;
import java.text.DateFormat;
import java.text.ParseException;
import java.util.ArrayList;
import java.util.Date;
import java.util.HashMap;
import java.util.Iterator;
import java.util.LinkedList;
import java.util.List;
import java.util.Map;

/**
 * Simplifies handling of command line arguments.
 * 
 * @author Corny
 * 
 */
public class Arguments {
	/**
	 * Plain arguments are arguments without a specifier like --output. They are
	 * usually used for input files.
	 * 
	 * @author Corny
	 * 
	 */
	public static enum PlainArgumentsType {
		/**
		 * No plain arguments are allowed
		 */
		NONE,
		/**
		 * One arbitrary plain argument is allowed
		 */
		ONE_ARBITRARY_ARGUMENT,
		/**
		 * One plain argument is allowed, it is used as the application name in
		 * the usage page
		 */
		ONE_ARGUMENT_AS_APPLICATION_NAME,
		/**
		 * Any number of arbitrary plain arguments is allowed
		 */
		LIST_OF_ARBITRARY_ARGUMENTS,
		/**
		 * Any number of arbitrary plain arguments is allowed, the first one is
		 * used as the application name in the usage page
		 */
		LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME;
	}

	private static abstract class ArgumentsOutputStream extends OutputStream {
		private final PrintStream sOut;
		private final ByteArrayOutputStream buffer = new ByteArrayOutputStream();
		private boolean isNewLine = true;
		private DateFormat dateFormat = DateFormat
				.getDateInstance(DateFormat.MEDIUM);
		private DateFormat timeFormat = DateFormat
				.getTimeInstance(DateFormat.MEDIUM);

		public ArgumentsOutputStream(boolean printToErrorStream) {
			this.sOut = printToErrorStream ? System.err : System.out;
		}

		@Override
		public void write(int b) throws IOException {
			if (printToLog()) {
				buffer.write(b);
			}
			if (printToConsole()) {
				sOut.write(b);
			}
		}

		@Override
		public void flush() throws IOException {
			sOut.flush();
			if (logOut != null && buffer.size() > 0) {
				byte[] byteArray = buffer.toByteArray();
				if (isNewLine) {
					Date date = new Date();
					logOut.write(("[" + dateFormat.format(date) + " "
							+ timeFormat.format(date) + "] ").getBytes());
					String prefix = getPrefix();
					if (prefix != null) {
						logOut.write(prefix.getBytes());
					}
				}

				logOut.write(byteArray);

				isNewLine = (byteArray[byteArray.length - 1] == '\n');
			}
			buffer.reset();
		}

		protected String getPrefix() {
			return null;
		}

		protected abstract boolean printToConsole();

		protected boolean printToLog() {
			return true;
		}
	}

	private static final class StdOutputStream extends ArgumentsOutputStream {
		public StdOutputStream() {
			super(false);
		}

		@Override
		protected boolean printToConsole() {
			return true;
		}
	}

	private static final class ErrOutputStream extends ArgumentsOutputStream {
		public ErrOutputStream() {
			super(true);
		}

		@Override
		protected boolean printToConsole() {
			return true;
		}

		@Override
		protected String getPrefix() {
			return "ERROR ";
		}
	}

	private static class LogOutputStream extends ArgumentsOutputStream {
		public LogOutputStream() {
			super(true);
		}

		@Override
		protected boolean printToConsole() {
			return verbose;
		}
	}

	private static final class ProgressOutputStream extends LogOutputStream {
		@Override
		protected boolean printToLog() {
			return false;
		}

		@Override
		protected boolean printToConsole() {
			return super.printToConsole() && !disableProgressOption.getValue();
		}
	}

	private static final String ONE_PLAIN_ARGUMENT_DESCRIPTION = "file";
	private static final String LIST_OF_PLAIN_ARGUMENTS_DESCRIPTION = "list of files";

	private static final Map<String, CommandLineOption<?>> longFormatToOption;
	private static final Map<Character, CommandLineOption<?>> shortRepresentationatToOption;
	private static final List<CommandLineOption<?>> options = new ArrayList<CommandLineOption<?>>(
			10);

	private static String applicationDescription;
	private static String verboseDescription = "Prints the values of all options";
	private static CommandLineOption<Boolean> helpOption, verboseOption,
			disableProgressOption;
	private static CommandLineOption<File> printBashCompletionOption;
	private static CommandLineOption<String> logOption;
	private static PlainArgumentsType plainArgumentsType = PlainArgumentsType.NONE;
	private static List<String> plainArguments;
	private static String plainArgumentsDescription;
	private static String applicationName;

	private static boolean verbose;
	private static FileOutputStream logOut;

	/**
	 * PrintStream for log messages
	 */
	public static final PrintStream log;

	/**
	 * PrintStream for progress indication which uses carriage return. Output
	 * will not be written to log file.
	 */
	public static final PrintStream progress;

	static {
		longFormatToOption = new HashMap<String, CommandLineOption<?>>();
		shortRepresentationatToOption = new HashMap<Character, CommandLineOption<?>>();
		helpOption = addBooleanOption('?', "help", "Displays this help page");
		printBashCompletionOption = addFileOption(
				null,
				"print-bash-completion",
				"Prints the bash completion script for this program to screen.",
				"PROGRAM", null, false, false).hide();
		log = new PrintStream(new LogOutputStream(), true);
		progress = new PrintStream(new ProgressOutputStream(), true);
	}

	private static void printHelpForOption(CommandLineOptionImpl<?> arg) {
		StringBuilder builder = new StringBuilder();
		builder.append(arg.getDescription());
		builder.append('\n');
		builder.append("Usage: ");
		if (applicationName != null) {
			builder.append(applicationName);
		} else {
			builder.append("[app]");
		}
		builder.append(" --");
		builder.append(arg.getLongRepresentation());
		if (arg.needsValue()) {
			builder.append(' ');
			builder.append(arg.getValueDescription());
		}
		builder.append('\n');
		arg.createHelpString(builder);

		System.out.println(builder);
	}

	private static void handlePlainArgument(String argString)
			throws IllegalArgumentException {
		if (printBashCompletionOption.getValue() != null) {
			// special case: ignore plain arguments type, we need the function
			// name as argument
			if (plainArguments == null) {
				plainArguments = new LinkedList<String>();
			}
			plainArguments.add(argString);
			return;
		}

		boolean oneArgumentWasSet = (applicationName != null)
				|| (plainArguments != null && plainArguments.size() > 0);

		if (oneArgumentWasSet
				&& (plainArgumentsType == PlainArgumentsType.ONE_ARBITRARY_ARGUMENT || plainArgumentsType == PlainArgumentsType.ONE_ARGUMENT_AS_APPLICATION_NAME)) {
			throw new IllegalArgumentException(
					"Only one plain argument is allowed.");
		}

		if (applicationName == null
				&& (plainArgumentsType == PlainArgumentsType.ONE_ARGUMENT_AS_APPLICATION_NAME || plainArgumentsType == PlainArgumentsType.LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME)) {
			applicationName = argString;
			return;
		}

		switch (plainArgumentsType) {
		case NONE:
			throw new IllegalArgumentException(
					"No plain arguments are allowed.");
		default:
			plainArguments.add(argString);
			break;
		}
	}

	private static final String valueToString(Object value) {
		if (value == null) {
			return "None";
		} else if (value instanceof Boolean) {
			return value == Boolean.TRUE ? "Yes" : "No";
		} else if (value instanceof String) {
			return "\"" + (String) value + "\"";
		} else {
			return value.toString();
		}
	}

	/**
	 * @param args
	 *            Args
	 * @return true to continue normally, false to abort immediately and return
	 *         false
	 * @throws ParseException
	 * @throws IllegalArgumentException
	 */
	@SuppressWarnings("unchecked")
	private static boolean doParse(String[] args) throws Exception {
		switch (plainArgumentsType) {
		case NONE:
			break;
		default:
			plainArguments = new LinkedList<String>();
		}

		Exception exception = null;

		verboseOption = addBooleanOption(null, "verbose", verboseDescription);
		logOption = addStringOption(null, "log", "File to print log to",
				"path", null);
		disableProgressOption = addBooleanOption(null, "disable-progress",
				"Disable progress information").hide();

		CommandLineOptionImpl<?> option = null;
		// remembers whether the last option was the help option, which
		// usually wants no value
		boolean helpNeeded = false;
		// only for debug reasons
		for (String argString : args) {
			try {
				if (option == null) {
					argString = argString.toLowerCase();

					// Searching for option
					if (argString.startsWith("-") && argString.length() > 1) {
						helpNeeded = false;
						// may be option
						if (argString.startsWith("--")) {
							// may be long format
							option = (CommandLineOptionImpl<?>) longFormatToOption
									.get(argString.substring(2));
						} else {
							// can only be short format
							option = (CommandLineOptionImpl<?>) shortRepresentationatToOption
									.get(argString.charAt(1));
						}

						if (option != null) {
							if (!option.needsValue()) {
								if (option == helpOption) {
									helpNeeded = true;
								}
								option.setValue(null);
								option = null;
							}
						} else {
							throw new IllegalArgumentException(
									"Unknown option \"" + argString + "\".");
						}
					} else {
						if (helpNeeded) {
							CommandLineOptionImpl<Boolean> helpOption = null;
							if (argString.length() == 1) {
								helpOption = (CommandLineOptionImpl<Boolean>) shortRepresentationatToOption
										.get(argString.charAt(0));
							}
							if (helpOption == null) {
								helpOption = (CommandLineOptionImpl<Boolean>) longFormatToOption
										.get(argString);
							}

							if (helpOption != null) {
								printHelpForOption(helpOption);
								return false;
							}
						}
						handlePlainArgument(argString);
						helpNeeded = false;
					}
				} else {
					if (option.value != null) {
						System.err
								.println("Warning: Option \""
										+ option.getLongRepresentation()
										+ "\" is set multiple times. Resulting value is "
										+ option.value + ".");
					} else {
						option.setValue(argString);
					}
					option = null;
				}
			} catch (Exception e) {
				if (exception == null) {
					exception = e;
				}
			}
		}
		if (option != null && option.needsValue() && option.getValue() == null
				&& exception == null) {
			exception = new IllegalArgumentException("Option \""
					+ option.getLongRepresentation() + "\" needs an argument.");
		}
		if (exception != null) {
			throw exception;
		}
		return true;
	}

	/**
	 * Initializes parser
	 * 
	 * @param args
	 *            Command line arguments
	 * @return true, if the Program should continue, false otherwise.
	 */
	public static boolean parse(String[] args) {
		if (plainArgumentsDescription == null) {
			switch (plainArgumentsType) {
			case ONE_ARBITRARY_ARGUMENT:
				plainArgumentsDescription = ONE_PLAIN_ARGUMENT_DESCRIPTION;
				break;
			case LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME:
			case LIST_OF_ARBITRARY_ARGUMENTS:
				plainArgumentsDescription = LIST_OF_PLAIN_ARGUMENTS_DESCRIPTION;
				break;
			}
		}

		try {
			if (!doParse(args)) {
				return false;
			}
		} catch (Exception e) {
			System.err.println(e.getLocalizedMessage());
			System.err.println();
			Arguments.printUsage();
			System.exit(-1);
		}

		if (helpOption.getValue()) {
			Arguments.printUsage();
			return false;
		}

		if (printBashCompletionOption.getValue() != null) {
			if (plainArguments.size() == 0) {
				System.err
						.println("Please specify the name of the bash completion function.");
				System.exit(-1);
			}
			printBashCompletionScript(printBashCompletionOption.getValue()
					.getPath(), plainArguments.get(0));
			return false;
		}

		try {
			if (logOption.getValue() != null) {
				logOut = new FileOutputStream((String) logOption.getValue());
			}
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}

		System.setOut(new PrintStream(new StdOutputStream(), true));
		System.setErr(new PrintStream(new ErrOutputStream(), true));

		verbose = verboseOption.getValue();
		log.println("State of all options:");
		for (CommandLineOption<?> opt : options) {
			if (opt == helpOption || opt == verboseOption) {
				continue;
			}
			CommandLineOptionImpl<?> option = (CommandLineOptionImpl<?>) opt;
			log.println("  " + option.getDescription() + ": "
					+ valueToString(option.getValue()));
		}
		log.println();

		return true;
	}

	/**
	 * Sets the handling of plain arguments.
	 * 
	 * @see PlainArgumentsType
	 * @param type
	 *            Type
	 */
	public static void setPlainArgumentsType(PlainArgumentsType type) {
		Arguments.plainArgumentsType = type;
	}

	/**
	 * @return The plain argument or the first one if there are multiple plain
	 *         arguments.
	 * @see PlainArgumentsType
	 */
	public static String getPlainArgument() {
		if (plainArgumentsType == PlainArgumentsType.NONE) {
			throw new UnsupportedOperationException(
					"No plain arguments are allowed.");
		}
		if (plainArguments.size() > 0) {
			return plainArguments.get(0);
		}
		return null;
	}

	/**
	 * @return The plain arguments.
	 * @see PlainArgumentsType
	 */
	public static List<String> getPlainArguments() {
		if (plainArgumentsType == PlainArgumentsType.NONE) {
			throw new UnsupportedOperationException(
					"No plain arguments are allowed.");
		}
		return plainArguments;
	}

	/**
	 * Sets the description text for plain arguments in the usage page. Usually,
	 * it says "file" or "list of files".
	 * 
	 * @param description
	 *            Description text
	 */
	public static void setPlainArgumentsDescription(String description) {
		switch (plainArgumentsType) {
		case ONE_ARGUMENT_AS_APPLICATION_NAME:
		case LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME:
			if (applicationName != null) {
				throw new IllegalStateException(
						"Cannot set the application name explicitly "
								+ "if the plain arguments specify the application name.");
			}
		}
		plainArgumentsDescription = description;
	}

	/**
	 * Sets the application's name in the usage page.
	 * 
	 * @param applicationName
	 *            Application name
	 */
	public static void setApplicationName(String applicationName) {
		switch (plainArgumentsType) {
		case ONE_ARGUMENT_AS_APPLICATION_NAME:
		case LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME:
			throw new IllegalStateException(
					"Cannot set the application name explicitly "
							+ "if the plain arguments specify the application name.");
		}
		Arguments.applicationName = applicationName;
	}

	private static void addOption(Character shortRepresentation,
			String longRepresentation, CommandLineOptionImpl<?> argument) {
		if (longRepresentation == null) {
			throw new NullPointerException("longRepresentation cannot be null.");
		}
		if (longFormatToOption.containsKey(longRepresentation)) {
			throw new IllegalArgumentException("Long representation '"
					+ longRepresentation + "' used multiple times.");
		}
		if (shortRepresentation != null) {
			if (shortRepresentationatToOption.containsKey(shortRepresentation)) {
				throw new IllegalArgumentException("Short representation '"
						+ shortRepresentation + "' used multiple times.");
			}
			shortRepresentationatToOption.put(shortRepresentation, argument);
		}
		longFormatToOption.put(longRepresentation, argument);
		options.add(argument);
	}

	/**
	 * Add a boolean command line argument
	 * 
	 * @param shortRepresentation
	 *            Short representation of the argument (optional)
	 * @param longRepresentation
	 *            Long representation of the argument (mandatory)
	 * @param description
	 *            Description of what the argument does
	 * @return The passed argument
	 */
	public static CommandLineOption<Boolean> addBooleanOption(
			Character shortRepresentation, String longRepresentation,
			String description) {
		BooleanOption argument = new BooleanOption(shortRepresentation,
				longRepresentation, description);
		addOption(shortRepresentation, longRepresentation, argument);
		return argument;
	}

	/**
	 * Add a integer command line argument
	 * 
	 * @param shortRepresentation
	 *            Short representation of the argument (optional)
	 * @param longRepresentation
	 *            Long representation of the argument (mandatory)
	 * @param description
	 *            Description of what the argument does
	 * @param valueDescription
	 *            Description of the value format
	 * @param defaultValue
	 *            Default value
	 * @return The passed argument
	 */
	public static CommandLineOption<Integer> addIntegerOption(
			Character shortRepresentation, String longRepresentation,
			String description, String valueDescription, Integer defaultValue) {
		IntegerOption argument = new IntegerOption(shortRepresentation,
				longRepresentation, description, valueDescription, defaultValue);
		addOption(shortRepresentation, longRepresentation, argument);
		return argument;
	}

	/**
	 * Add a double command line argument
	 * 
	 * @param shortRepresentation
	 *            Short representation of the argument (optional)
	 * @param longRepresentation
	 *            Long representation of the argument (mandatory)
	 * @param description
	 *            Description of what the argument does
	 * @param valueDescription
	 *            Description of the value format
	 * @param defaultValue
	 *            Default value
	 * @return The passed argument
	 */
	public static CommandLineOption<Double> addDoubleOption(
			Character shortRepresentation, String longRepresentation,
			String description, String valueDescription, Double defaultValue) {
		DoubleOption argument = new DoubleOption(shortRepresentation,
				longRepresentation, description, valueDescription, defaultValue);
		addOption(shortRepresentation, longRepresentation, argument);
		return argument;
	}

	/**
	 * Add a String command line argument
	 * 
	 * @param shortRepresentation
	 *            Short representation of the argument (optional)
	 * @param longRepresentation
	 *            Long representation of the argument (mandatory)
	 * @param description
	 *            Description of what the argument does
	 * @param valueDescription
	 *            Description of the value format
	 * @param defaultValue
	 *            Default value
	 * @return The passed argument
	 */
	public static CommandLineOption<String> addStringOption(
			Character shortRepresentation, String longRepresentation,
			String description, String valueDescription, String defaultValue) {
		StringOption argument = new StringOption(shortRepresentation,
				longRepresentation, description, valueDescription, defaultValue);
		addOption(shortRepresentation, longRepresentation, argument);
		return argument;
	}

	/**
	 * Add a File command line argument
	 * 
	 * @param shortRepresentation
	 *            Short representation of the argument (optional)
	 * @param longRepresentation
	 *            Long representation of the argument (mandatory)
	 * @param description
	 *            Description of what the argument does
	 * @param valueDescription
	 *            Description of the value format
	 * @param defaultValue
	 *            Default value
	 * @param ensureFileExists
	 *            true to only accept existing files
	 * @param isDirectory
	 *            If ensureFileExists is true, this value specifies whether to
	 *            check for a file or a directory at the specified path.
	 * @return The passed argument
	 */
	public static CommandLineOption<File> addFileOption(
			Character shortRepresentation, String longRepresentation,
			String description, String valueDescription, File defaultValue,
			boolean ensureFileExists, boolean isDirectory) {
		FileOption argument = new FileOption(shortRepresentation,
				longRepresentation, description, valueDescription,
				defaultValue, ensureFileExists, isDirectory);
		addOption(shortRepresentation, longRepresentation, argument);
		return argument;
	}

	/**
	 * Add a choice command line argument. It has a finite set of possible
	 * String values.
	 * 
	 * @param <T>
	 *            Argument type
	 * 
	 * @param shortRepresentation
	 *            Short representation of the argument (optional)
	 * @param longRepresentation
	 *            Long representation of the argument (mandatory)
	 * @param description
	 *            Description of what the argument does
	 * @param valueDescription
	 *            Description of the value format
	 * @param possibilities
	 *            The possible values for the argument. Ensure that
	 *            {@link T#toString()} returns a human readable, unique
	 *            representation.
	 * @param defaultValue
	 *            Default value
	 * @return The passed argument
	 */
	public static <T> CommandLineOption<T> addChoiceOption(
			Character shortRepresentation, String longRepresentation,
			String description, String valueDescription, T[] possibilities,
			T defaultValue) {
		ChoiceOption<T> argument = new ChoiceOption<T>(shortRepresentation,
				longRepresentation, description, valueDescription,
				possibilities, defaultValue);
		addOption(shortRepresentation, longRepresentation, argument);
		return argument;
	}

	/**
	 * Sets the short description about what the application does.
	 * 
	 * @param description
	 *            Description
	 */
	public static void setApplicationDescription(String description) {
		Arguments.applicationDescription = description;
	}

	/**
	 * Sets the description text for the verbose option. Useful if you use the
	 * verbose option for the behavior of the program.
	 * 
	 * @param verboseDescription
	 *            Description text for the verbose option
	 */
	public static void setVerboseDescription(String verboseDescription) {
		Arguments.verboseDescription = verboseDescription;
	}

	/**
	 * @return The verbose option.
	 */
	public static CommandLineOption<Boolean> getVerboseOption() {
		return verboseOption;
	}

	/**
	 * Prints the usage to a PrintStream.
	 * 
	 * @param out
	 *            PrintStream to write usage to
	 */
	public static void printUsage(PrintStream out) {
		StringBuilder builder = new StringBuilder();
		if (applicationDescription != null) {
			builder.append(applicationDescription);
			builder.append("\n\n");
		}
		builder.append("Usage: ");
		if (applicationName != null) {
			builder.append(applicationName);
		} else {
			builder.append("[app]");
		}
		builder.append(" [list of arguments]");
		switch (plainArgumentsType) {
		case ONE_ARBITRARY_ARGUMENT:
		case LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME:
		case LIST_OF_ARBITRARY_ARGUMENTS:
			builder.append(' ');
			builder.append(plainArgumentsDescription);
		}
		out.println(builder.toString());
		out.println();

		out.println("Arguments:");

		int maxLength = 0;
		List<String> output = new LinkedList<String>();
		for (CommandLineOption<?> argument : options) {
			CommandLineOptionImpl<?> arg = (CommandLineOptionImpl<?>) argument;
			if (arg.isHidden()) {
				continue;
			}
			builder = new StringBuilder();
			if (arg.getShortRepresentation() != null) {
				builder.append("  [--");
			} else {
				builder.append("  --");
			}
			builder.append(arg.getLongRepresentation());
			if (arg.getShortRepresentation() != null) {
				builder.append(" | -");
				builder.append(arg.getShortRepresentation());
				builder.append(']');
			}
			if (arg.needsValue()) {
				builder.append(' ');
				if (arg.getValueDescription() != null) {
					builder.append(arg.getValueDescription());
				}
			}

			String outputString = builder.toString();
			output.add(outputString);
			if (outputString.length() > maxLength) {
				maxLength = outputString.length();
			}
		}

		Iterator<String> outputStringIt = output.iterator();
		Iterator<CommandLineOption<?>> argIt = options.iterator();

		while (argIt.hasNext()) {
			CommandLineOptionImpl<?> argument = (CommandLineOptionImpl<?>) argIt
					.next();
			if (argument.isHidden()) {
				continue;
			}
			String left = outputStringIt.next();

			StringBuilder build = new StringBuilder("%-");
			build.append(maxLength);
			build.append('.');
			build.append(maxLength);
			build.append("s   ");
			build.append(argument.getDescription());
			build.append('.');
			if (!(argument instanceof BooleanOption)
					&& argument.defaultValue != null) {
				build.append(" Defaults to ");
				build.append(valueToString(argument.defaultValue));
				build.append(".");
			}
			build.append("%n");

			out.format(build.toString(), left);
		}
	}

	/**
	 * Prints the usage to the console
	 */
	public static void printUsage() {
		printUsage(System.out);
	}

	private static void printBashCompletionScript(String progName,
			String functionName) {
		if (options.size() == 0) {
			return;
		}

		StringBuilder build = new StringBuilder(functionName);
		build.append("() {\n");

		// Variables
		build.append("\tCOMPREPLY=()\n");
		build.append("\tcur=\"${COMP_WORDS[COMP_CWORD]}\"\n");
		build.append("\tprev=\"${COMP_WORDS[COMP_CWORD-1]}\"\n\n");

		// Long representations
		build.append("\tlong_opts=\"");
		boolean first = true;
		for (CommandLineOption<?> argument : options) {
			CommandLineOptionImpl<?> arg = (CommandLineOptionImpl<?>) argument;
			if (first) {
				first = false;
			} else {
				build.append(' ');
			}
			build.append("--");
			build.append(arg.getLongRepresentation());
		}
		build.append("\"\n\n");

		// argument handling
		// case header
		build.append("\tcase \"${prev}\" in\n");
		for (CommandLineOption<?> argument : options) {
			CommandLineOptionImpl<?> arg = (CommandLineOptionImpl<?>) argument;
			// handle choice
			build.append("\t\"--");
			build.append(arg.getLongRepresentation());
			if (arg.getShortRepresentation() != null) {
				build.append("\" | \"-");
				build.append(arg.getShortRepresentation());
			}
			build.append("\")\n");
			if (arg.needsValue() || arg == helpOption) {
				build.append("\t\t");
				if (arg == helpOption) {
					// expand argument representations
					createHelpCompletionLine(build);
				} else {
					arg.createBashCompletionCase(build);
				}
				build.append("\n\t\treturn 0\n");
			}
			// end current case
			build.append("\t\t;;\n");
		}
		// case footer
		build.append("\tesac\n\n");

		// Complete representations
		// case header
		build.append("\tcase \"${cur}\" in\n");
		// all representations
		build.append("\t-*)\n");
		build.append("\t\tCOMPREPLY=($(compgen -W \"${long_opts}\" -- ${cur}))\n");
		build.append("\t\treturn 0\n\t\t;;\n");

		// case footer
		build.append("\tesac\n");

		build.append("}\n");

		// complete call
		build.append("complete ");
		switch (plainArgumentsType) {
		case LIST_FIRST_ARGUMENT_AS_APPLICATION_NAME:
		case LIST_OF_ARBITRARY_ARGUMENTS:
		case ONE_ARBITRARY_ARGUMENT:
			build.append("-o filenames -o dirnames ");
			break;
		}
		build.append("-F " + functionName + " " + progName);

		System.out.println(build);
	}

	private static void createHelpCompletionLine(StringBuilder build) {
		build.append("COMPREPLY=($(compgen -W \"");
		boolean first = true;
		for (CommandLineOption<?> argument2 : options) {
			CommandLineOptionImpl<?> arg2 = (CommandLineOptionImpl<?>) argument2;
			if (first) {
				first = false;
			} else {
				build.append(' ');
			}
			build.append(arg2.getLongRepresentation());
		}
		build.append("\" -- ${cur}))");
	}

	/**
	 * Test Program
	 * 
	 * @param args
	 */
	public static void main(String[] args) {
		addIntegerOption('i', "int", "some int", "number", 42);
		addChoiceOption('c', "choice", "Choose wisely.", "type", new String[] {
				"banana", "mango", "apple" }, 0);
		addStringOption('s', "string", "A String", "string", "Asdf asdf Asdf");
		addFileOption('f', "file", "A file", "FILE", null, true, false);

		if (!parse("--help c".split(" "))) {
			return;
		}

		System.out.println("Test via system out");
		System.err.println("Test via system err");
		System.out.println("Test via Arguments.out");
		log.println("Test via Arguments.log");
	}
}
