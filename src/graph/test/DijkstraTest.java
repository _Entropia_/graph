package graph.test;

import graph.defaultgraph.DefaultEdge;
import graph.defaultgraph.DefaultGraph;
import graph.defaultgraph.DefaultVertex;
import graph.distance.DistanceUtilities;

/**
 * Runs Dijkstra's algorithm on a simple graph taken from
 * https://en.wikipedia.org/wiki/Dijkstra%27s_algorithm#Algorithm
 * 
 * @author Corny
 */
public class DijkstraTest {
	/**
	 * Test program
	 * 
	 * @param args
	 *            ignored
	 */
	public static void main(String[] args) {
		DefaultGraph<Integer, DefaultEdge<Integer>, DefaultVertex<Integer, DefaultEdge<Integer>>> graph = new DefaultGraph<>();
		DefaultVertex<Integer, DefaultEdge<Integer>> v1 = new DefaultVertex<Integer, DefaultEdge<Integer>>(1);
		DefaultVertex<Integer, DefaultEdge<Integer>> v2 = new DefaultVertex<Integer, DefaultEdge<Integer>>(2);
		DefaultVertex<Integer, DefaultEdge<Integer>> v3 = new DefaultVertex<Integer, DefaultEdge<Integer>>(3);
		DefaultVertex<Integer, DefaultEdge<Integer>> v4 = new DefaultVertex<Integer, DefaultEdge<Integer>>(4);
		DefaultVertex<Integer, DefaultEdge<Integer>> v5 = new DefaultVertex<Integer, DefaultEdge<Integer>>(5);
		DefaultVertex<Integer, DefaultEdge<Integer>> v6 = new DefaultVertex<Integer, DefaultEdge<Integer>>(6);

		graph.addVertex(v1);
		graph.addVertex(v2);
		graph.addVertex(v3);
		graph.addVertex(v4);
		graph.addVertex(v5);
		graph.addVertex(v6);

		v1.addEdge(new DefaultEdge<>(v1, v2, 7.));
		v2.addEdge(new DefaultEdge<>(v2, v1, 7.));

		v1.addEdge(new DefaultEdge<>(v1, v3, 9.));
		v3.addEdge(new DefaultEdge<>(v3, v1, 9.));

		v1.addEdge(new DefaultEdge<>(v1, v6, 14.));
		v6.addEdge(new DefaultEdge<>(v6, v1, 14.));

		v2.addEdge(new DefaultEdge<>(v2, v3, 10.));
		v3.addEdge(new DefaultEdge<>(v3, v2, 10.));

		v2.addEdge(new DefaultEdge<>(v2, v4, 15.));
		v4.addEdge(new DefaultEdge<>(v4, v2, 15.));

		v3.addEdge(new DefaultEdge<>(v3, v4, 11.));
		v4.addEdge(new DefaultEdge<>(v4, v3, 11.));

		v3.addEdge(new DefaultEdge<>(v3, v6, 2.));
		v6.addEdge(new DefaultEdge<>(v6, v3, 2.));

		v4.addEdge(new DefaultEdge<>(v4, v5, 6.));
		v5.addEdge(new DefaultEdge<>(v5, v4, 6.));

		v5.addEdge(new DefaultEdge<>(v5, v6, 9.));
		v6.addEdge(new DefaultEdge<>(v6, v5, 9.));

		DistanceUtilities.dijkstra(graph, v1);
		System.out.println(graph);
	}
}
