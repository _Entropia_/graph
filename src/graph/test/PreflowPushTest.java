package graph.test;

import java.io.File;
import java.io.IOException;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.preflowpush.ExcessScalingPreflowPush;
import graph.flow.preflowpush.FIFOPreflowPush;
import graph.flow.preflowpush.FirstPreflowPush;
import graph.flow.preflowpush.HighestLabelPreflowPush;
import graph.flow.preflowpush.PreflowPush;
import graph.flow.preflowpush.RandomVertexPreflowPush;
import graph.flow.preflowpush.TreeExcessScalingPreflowPush;
import graph.test.args.Arguments;
import graph.test.args.Arguments.PlainArgumentsType;
import graph.test.args.CommandLineOption;

/**
 * Test program for Preflow-Push algorithms
 * 
 * @author Corny
 */
public class PreflowPushTest {
	private enum AugmentingPathsMethod {
		RANDOM_VERTEX {
			@Override
			public <T extends Comparable<T>> PreflowPush<T> getAlgo(
					FlowNetwork<T> network) {
				return new RandomVertexPreflowPush<T>(network, Arguments.log);
			}
		},
		FIFO {
			@Override
			public <T extends Comparable<T>> PreflowPush<T> getAlgo(
					FlowNetwork<T> network) {
				return new FIFOPreflowPush<T>(network, Arguments.log);
			}
		},
		FIRST {
			@Override
			public <T extends Comparable<T>> PreflowPush<T> getAlgo(
					FlowNetwork<T> network) {
				return new FirstPreflowPush<T>(network, Arguments.log);
			}
		},
		HIGHESTLABEL {
			@Override
			public <T extends Comparable<T>> PreflowPush<T> getAlgo(
					FlowNetwork<T> network) {
				return new HighestLabelPreflowPush<T>(network, Arguments.log);
			}
		},
		EXCESS_SCALING {
			@Override
			public <T extends Comparable<T>> PreflowPush<T> getAlgo(
					FlowNetwork<T> network) {
				return new ExcessScalingPreflowPush<T>(network, Arguments.log);
			}
		},
		TREE_EXCESS_SCALING {
			@Override
			public <T extends Comparable<T>> PreflowPush<T> getAlgo(
					FlowNetwork<T> network) {
				return new TreeExcessScalingPreflowPush<T>(network,
						Arguments.log);
			}
		};

		public abstract <T extends Comparable<T>> PreflowPush<T> getAlgo(
				FlowNetwork<T> network);
	}

	private static final CommandLineOption<AugmentingPathsMethod> pushAlgoOption = Arguments
			.addChoiceOption('a', "algo", "Preflow-Push algorithm", "algo",
					AugmentingPathsMethod.values(),
					AugmentingPathsMethod.TREE_EXCESS_SCALING);
	private static final CommandLineOption<File> inFileOption = Arguments
			.addFileOption('i', "input",
					"Input file containing the graph data", "FILE", new File(
							"data/graph1.txt"), true, false);

	/**
	 * Preflow Push test program
	 * 
	 * @param args
	 *            none
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Arguments
				.setPlainArgumentsType(PlainArgumentsType.ONE_ARGUMENT_AS_APPLICATION_NAME);
		if (!Arguments.parse(args)) {
			return;
		}

		File file = inFileOption.getValue();
		FlowNetwork<Integer> network = FlowUtilities.readFlowNetwork(file);

		PreflowPush<Integer> preflowPush = pushAlgoOption.getValue().getAlgo(
				network);

		System.out.println("Running algorithm " + pushAlgoOption.getValue());

		FlowUtilities.preflowPush(network, preflowPush);

		double flow = 0.;
		for (FlowEdge<Integer> edge : network.getSource()) {
			flow += edge.getFlow();
		}

		System.out.println("Fluss ist " + flow + ", "
				+ preflowPush.getLiftCount() + " Lift-Operationen und "
				+ preflowPush.getPushCount() + " Push-Operationen benötigt.");
	}
}
