package graph.test;

import graph.flow.FlowEdge;
import graph.flow.FlowNetwork;
import graph.flow.FlowUtilities;
import graph.flow.fordfulkerson.AugmentingPathFinder;
import graph.flow.fordfulkerson.BFSAugmentingPathFinder;
import graph.flow.fordfulkerson.DFSAugmentingPathFinder;
import graph.flow.fordfulkerson.ScalingAugmentingPathFinder;
import graph.test.args.Arguments;
import graph.test.args.Arguments.PlainArgumentsType;
import graph.test.args.CommandLineOption;

import java.io.File;
import java.io.IOException;

/**
 * Test progam for the Ford-Fulkerson algorithm
 * 
 * @author Corny
 */
public class FordFulkersonTest {
	private enum AugmentingPathsMethod {
		BFS {
			@Override
			public <T extends Comparable<T>> AugmentingPathFinder<T> getFinder(FlowNetwork<T> network) {
				return new BFSAugmentingPathFinder<T>(network);
			}
		},
		DFS {
			@Override
			public <T extends Comparable<T>> AugmentingPathFinder<T> getFinder(FlowNetwork<T> network) {
				return new DFSAugmentingPathFinder<T>(network);
			}
		},
		SCALING {
			@Override
			public <T extends Comparable<T>> AugmentingPathFinder<T> getFinder(FlowNetwork<T> network) {
				return new ScalingAugmentingPathFinder<T>(network, 1);
			}
		};

		public abstract <T extends Comparable<T>> AugmentingPathFinder<T> getFinder(
				FlowNetwork<T> network);
	}

	private static final CommandLineOption<AugmentingPathsMethod> pathMethodOption = Arguments
			.addChoiceOption('f', "finder", "Augmenting paths finding method",
					"algo", AugmentingPathsMethod.values(),
					AugmentingPathsMethod.BFS);
	private static final CommandLineOption<File> inFileOption = Arguments
			.addFileOption('i', "input",
					"Input file containing the graph data", "FILE", new File(
							"data/graph1.txt"), true, false);

	/**
	 * Ford-Fulkerson test program
	 * 
	 * @param args
	 *            none
	 * @throws IOException
	 */
	public static void main(String[] args) throws IOException {
		Arguments
				.setPlainArgumentsType(PlainArgumentsType.ONE_ARGUMENT_AS_APPLICATION_NAME);
		if (!Arguments.parse(args)) {
			return;
		}

		File file = inFileOption.getValue();
		FlowNetwork<Integer> network = FlowUtilities.readFlowNetwork(file);

		int paths = FlowUtilities.fordFulkerson(network, pathMethodOption
				.getValue().getFinder(network), Arguments.log);

		double flow = 0.;
		for (FlowEdge<Integer> edge : network.getSource()) {
			flow += edge.getFlow();
		}
		System.out.println("Fluss ist " + flow + ", " + paths
				+ " augmentierende Wege berechnet.");
	}
}
