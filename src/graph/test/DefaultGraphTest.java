package graph.test;

import graph.defaultgraph.DefaultEdge;
import graph.defaultgraph.DefaultGraph;
import graph.defaultgraph.DefaultVertex;
import graph.utils.GraphUtilities;

/**
 * Tests initialization of a default graph, as well as printing a graph.
 * 
 * @author Corny
 */
public class DefaultGraphTest {
	/**
	 * Test program
	 * 
	 * @param args
	 *            none
	 */
	public static void main(String[] args) {
		DefaultGraph<Integer, DefaultEdge<Integer>, DefaultVertex<Integer, DefaultEdge<Integer>>> g = new DefaultGraph<Integer, DefaultEdge<Integer>, DefaultVertex<Integer, DefaultEdge<Integer>>>();

		DefaultVertex<Integer, DefaultEdge<Integer>> one = new DefaultVertex<Integer, DefaultEdge<Integer>>(1);
		DefaultVertex<Integer, DefaultEdge<Integer>> two = new DefaultVertex<Integer, DefaultEdge<Integer>>(2);
		DefaultVertex<Integer, DefaultEdge<Integer>> three = new DefaultVertex<Integer, DefaultEdge<Integer>>(3);
		DefaultVertex<Integer, DefaultEdge<Integer>> four = new DefaultVertex<Integer, DefaultEdge<Integer>>(4);
		DefaultVertex<Integer, DefaultEdge<Integer>> five = new DefaultVertex<Integer, DefaultEdge<Integer>>(5);

		one.addEdge(new DefaultEdge<Integer>(one, two, 1));
		one.addEdge(new DefaultEdge<Integer>(one, four, 1));
		two.addEdge(new DefaultEdge<Integer>(two, five, 1));
		two.addEdge(new DefaultEdge<Integer>(two, three, 1));
		three.addEdge(new DefaultEdge<Integer>(three, four, 1));
		four.addEdge(new DefaultEdge<Integer>(four, five, 1));
		five.addEdge(new DefaultEdge<Integer>(five, four, 1));
		five.addEdge(new DefaultEdge<Integer>(five, three, 1));

		g.addVertex(one);
		g.addVertex(two);
		g.addVertex(three);
		g.addVertex(four);
		g.addVertex(five);

		System.out.println(g);
		System.out.println();

		GraphUtilities.breadthFirstSearch(g, one, null);

		System.out.println(g);

	}
}
