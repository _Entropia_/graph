package graph;

/**
 * Interface for (directed) graphs
 * 
 * @author Corny
 * 
 * @param <T>
 *            Label type
 * @param <EDGE>
 *            Edge type
 * @param <VERTEX>
 *            Vertex type
 */
public interface Graph<T, EDGE extends Edge<T>, VERTEX extends Vertex<T, EDGE>>
		extends Iterable<VERTEX> {
	/**
	 * @return The number of vertices in this Graph.
	 */
	int getVertexCount();
}