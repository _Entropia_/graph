#!/bin/bash

JAVA_BIN=java
JAVA_OPTS=-Xmx1g
MAIN=graph.test.PreflowPushTest

export CLASSPATH=bin

$JAVA_BIN $JAVA_OPTS $MAIN "$0" "$@"
